----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 19:22:37
-- Design Name: 
-- Module Name: SOPC - BehavSOPC
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.Consts.all;

entity SOPC is
	generic ( infile : in string;
           stdfile : in string);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           int: in STD_LOGIC_VECTOR(5 downto 0);
           timer_int: out STD_LOGIC);
end SOPC;

architecture BehavSOPC of SOPC is
    signal ramMode: T_RAMMode;
    signal ramWData: DWord;
    signal ramAddr: DWord;
    signal ramRData: DWord;

begin
	uMIPSCPU: entity work.MIPS_CPU port map(
		clk => clk,
		rst => rst,
		int => int,

        ramMode => ramMode,
        ramWdata => ramWData,
        ramAddr => ramAddr,
        ramRData => ramRData,

        timer_int => timer_int
    );

    uRAM: entity work.RAM generic map (
        infile => infile
    ) port map (
        clk => clk,
        rst => rst,
        addr => ramAddr,
        rdata => ramRData,
        wdata => ramWData,
        mode => ramMode
    );

end BehavSOPC;
