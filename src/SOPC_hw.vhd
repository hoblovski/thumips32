----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 19:22:37
-- Design Name: 
-- Module Name: SOPC - BehavSOPC
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.Consts.all;

entity SOPC_HW is
    Port (
           -- misc 
           touch_btn: in std_logic_vector(5 downto 0);
           clk: in std_logic;
           clk_uart: in std_logic;
           leds: out std_logic_vector(31 downto 0);
           dip_sw: in std_logic_vector(31 downto 0);
           -- com
           txd: out std_logic;
           rxd: in std_logic;
           -- ram
           base_ram_addr: out std_logic_vector(19 downto 0);
           base_ram_data: inout std_logic_vector(31 downto 0);
           base_ram_be_n: out std_logic_vector(3 downto 0);
           base_ram_ce_n: out std_logic;
           base_ram_oe_n: out std_logic;
           base_ram_we_n: out std_logic;
           ext_ram_addr: out std_logic_vector(19 downto 0);
           ext_ram_data: inout std_logic_vector(31 downto 0);
           ext_ram_be_n: out std_logic_vector(3 downto 0);
           ext_ram_ce_n: out std_logic;
           ext_ram_oe_n: out std_logic;
           ext_ram_we_n: out std_logic;
           -- flash
           flash_a : out STD_LOGIC_VECTOR (22 downto 0);
           flash_rp_n : out STD_LOGIC;
           flash_vpen : out STD_LOGIC;
           flash_oe_n : out STD_LOGIC;
           flash_we_n : out STD_LOGIC;
           flash_ce_n : out STD_LOGIC;
           flash_byte_n : out STD_LOGIC;
           flash_data : inout STD_LOGIC_VECTOR (15 downto 0)
        ); 
end SOPC_hw;

architecture BehavSOPC of SOPC_HW is
    component async_transmitter generic(
        ClkFrequency: integer;
        Baud: integer);
    port(
        clk: in std_logic;
        TxD_start: in std_logic;
        TxD_data: in std_logic_vector(7 downto 0);
        TxD: out std_logic;
        TxD_busy: out std_logic;
        TxD_state_test: out std_logic_vector(3 downto 0)
    );
    end component;
    component async_receiver generic(
        ClkFrequency: integer;
        Baud: integer);
    port(
        clk: in std_logic;
        RxD_data: out std_logic_vector(7 downto 0);
        RxD: in std_logic;
        RxD_idle: out std_logic;
        RxD_endofpacket: out std_logic;
        RxD_data_ready: out std_logic
    );
    end component;
    
    signal ramMode: T_RAMMode;
    signal ramWData: DWord;
    signal ramAddr: DWord;
    signal ramRData: DWord;

    signal MEMStallTEST: std_logic;

    signal rst: std_logic;
    signal int: STD_LOGIC_VECTOR(5 downto 0);
    signal timer_int: STD_LOGIC;

    signal IFpc: DWord;
    signal inst: DWord;
    signal bflag: std_logic;

    signal l_clk: std_logic;
    signal ll_clk: std_logic;
    signal lll_clk:std_logic;
    signal lll_clk_flash: std_logic;
    signal watchedReg: DWord;

    signal l_leds: std_logic_vector(31 downto 0);
    
    signal flashAddr: std_logic_vector(31 downto 0);
    signal flashre, flashpause: std_logic;
    signal flashData: std_logic_vector(15 downto 0);

    signal COMReceiveData: std_logic_vector(7 downto 0);
    signal COMReceiveIdle, COMReceiveEndofPacket, COMReceiveDataReady: std_logic;
    signal COMTransmitData: std_logic_vector(7 downto 0); 
    signal COMTransmitStart, COMTransmitBusy: std_logic;
    
    signal COMTransmitStateTEST: std_logic_vector(3 downto 0);
              signal COMReadyReadTEST: std_logic;
              signal COMLastReadTEST: std_logic;
              
              signal COMRDataTEST: std_logic_vector(7 downto 0);
              signal epcTEST: std_logic_vector(31 downto 0);
              signal causeTEST: std_logic_vector(31 downto 0);
              signal statusTEST: std_logic_vector(31 downto 0);
              signal newPCTEST: std_logic_vector(31 downto 0);
              signal flushTEST: std_logic;
              signal BranchAddrTEST: Dword;
              signal watchTLB: std_logic_vector(63 downto 0);
              signal watchTLBwe: std_logic_vector(5 downto 0);
              signal watchCP0Index: std_logic_vector(31 downto 0);
              signal pcMoveImm_SignedExtendTEST: std_logic_vector(31 downto 0);
              signal pcPlus4TEST: std_logic_vector(31 downto 0);
              signal IDinstTEST: std_logic_vector(31 downto 0);
              signal ERRORaddrTEST: std_logic_vector(31 downto 0);

begin

    process (clk)
    begin
        if (rising_edge(clk)) then
            l_clk <= not l_clk;
        end if;
    end process;
    
    process(l_clk)
    begin
        if (rising_edge(l_clk)) then
            ll_clk <= not ll_clk;
        end if;
     end process;

     process(ll_clk)
     begin
         if (rising_edge(ll_clk)) then
             lll_clk <= not lll_clk;
         end if;
      end process;
      
      process(ll_clk)
      begin
          if (falling_edge(ll_clk)) then
              lll_clk_flash <= lll_clk;
          end if;
       end process;

    rst <= touch_btn(5);
    int <= (2=> (comReadyReadTEST xor comLastReadTEST),
            5=> timer_int,
            others=> '0');

    --leds <=  watchTLB(22 downto 0) & watchCP0Index(2 downto 0) & watchTLBwe;
    --leds <= watchedReg(31 downto 16) & inst(7 downto 0) & IFPC(7 downto 1) & MEMStallTEST;
    leds <= ERRORaddrTEST;

	uMIPSCPU: entity work.MIPS_CPU port map(
		clk => (lll_clk and dip_sw(0)) or touch_btn(4),
		romCLK => clk,
		--clk => touch_btn(4),
		rst => rst,
        ramMode => ramMode,
        ramWdata => ramWData,
        ramAddr => ramAddr,
        ramRData => ramRData,
        COMReceiveData => COMReceiveData,
        COMTransmitData => COMTransmitData,
        COMTransmitStart => COMTransmitStart,
        COMReceiveReady => COMReceiveDataReady,
        COMTransmitBusy => COMTransmitBusy,
        FlashRe => FlashRe,
        FlashAddr => FlashAddr,
        FlashPause => FlashPause,
        FlashData => x"0000" & FlashData,
		int => int,
        timer_int => timer_int,
        bootFromFlash => dip_sw(31),
        watchedReg => watchedReg,
        pc => IFpc,
        inst => inst,
        watchTLB => watchTLB,
        watchTLBwe => watchTLBwe,
        watchCP0Index => watchCP0Index,
                           COMReadyReadTEST => COMReadyReadTEST,
                           COMLastReadTEST => COMLastReadTEST,
                           COMRDataTEST => COMRDataTEST,
                           epcTEST => epcTEST,
                           causeTEST => causeTEST,
                           statusTEST => statusTEST,
                           newPCTEST => newPCTEST,
                           flushTEST=>flushTEST,
                           MEMStallTEST => MEMStallTEST,
                           BranchAddrTEST => BranchAddrTEST,
                           pcMoveImm_SignedExtendTEST => pcMoveImm_SignedExtendTEST,
                           pcPlus4TEST => pcPlus4TEST,
                           IDinstTEST => IDinstTEST,
                           ERRORaddrTEST =>ERRORaddrTEST
    );

    uRAM: entity work.RamController port map (
		clk => lll_clk_flash,
        -- used in CPU internal interaction
        vaddr => ramAddr,
        rdata => ramRData,
        wdata => ramWData,
        mode => ramMode,
        -- below are connected to I/O ports
        base_ram_addr => base_ram_addr,
        base_ram_data => base_ram_data,
        base_ram_be_n => base_ram_be_n,
        base_ram_ce_n => base_ram_ce_n,
        base_ram_oe_n => base_ram_oe_n,
        base_ram_we_n => base_ram_we_n,
        ext_ram_addr => ext_ram_addr,
        ext_ram_data => ext_ram_data,
        ext_ram_be_n => ext_ram_be_n,
        ext_ram_ce_n => ext_ram_ce_n,
        ext_ram_oe_n => ext_ram_oe_n,
        ext_ram_we_n => ext_ram_we_n
    );
    
    uAsyncTransmitter: component async_transmitter generic map(
        ClkFrequency => 11059200,
        Baud => 115200
    )
    port map(
        clk => clk_uart,
        TxD => txd,
        TxD_start => COMTransmitStart,
        TxD_data => COMTransmitData,
        TxD_busy => COMTransmitBusy,
        TxD_state_test => COMTransmitStateTEST
    );
       
    uAsyncReceiver: async_receiver generic map(
        ClkFrequency => 11059200,
        Baud => 115200
    )
    port map(
        clk => clk_uart,
        RxD => rxd,
        RxD_data_ready => COMReceiveDataReady,
        RxD_data => COMReceiveData,
        RxD_idle => COMReceiveIdle,
        RxD_endofpacket => COMReceiveEndofPacket
    );
    
    uFlashController:entity work.FlashController port map(
        rst => rst,
        clk => lll_clk_flash,
        addr => flashAddr,
        re => flashre,
        pause => flashpause,
        data => flashData,
        flash_a => flash_a,
        flash_rp_n => flash_rp_n,
        flash_vpen => flash_vpen,
        flash_oe_n => flash_oe_n,
        flash_we_n => flash_we_n,
        flash_ce_n => flash_ce_n,
        flash_byte_n => flash_byte_n,
        flash_data => flash_data
    );
        

end BehavSOPC;
