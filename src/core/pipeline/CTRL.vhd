----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/31 21:01:13
-- Design Name: 
-- Module Name: ctrl - Behav_ctrl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.consts.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ctrl is
    Port ( rst : in STD_LOGIC;
           cp0Epc : in STD_LOGIC_VECTOR (31 downto 0);
           exceptType : in STD_LOGIC_VECTOR (31 downto 0);
           newPC : out STD_LOGIC_VECTOR (31 downto 0);
           flush : out STD_LOGIC
           );
end ctrl;

architecture Behav_ctrl of ctrl is

begin
    process (all) 
    begin
        if (rst = RstEnable) then
            flush <= '0';
            newPC <= Zero32;
        elsif (not (exceptType = Zero32)) then
            flush <= '1';
            case (exceptType) is
                when Except_Interrupt =>
                    newPC <= x"8002a000";
                when Except_Syscall =>
                    newPC <= x"8002a000";
                when Except_InstInvalid =>
                    newPC <= x"8002a000";
                when Except_TLBstore =>
                    newPC <= x"8002a000";
                when Except_TLBload =>
                    newPC <= x"8002a000";
                when Except_Eret =>
                    newPC <= cp0Epc;
                when others =>
            end case;
        else
            flush <= '0'; 
            newPC <= Zero32;
        end if;
    end process;

end Behav_ctrl;
