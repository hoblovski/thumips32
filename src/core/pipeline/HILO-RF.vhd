----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 11:26:46
-- Design Name: 
-- Module Name: Register - BehavReg
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
--      implements HI/LO registers.
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HiLoRegFile is
    port ( rst: in std_logic;
           clk: in std_logic;
           wE: in std_logic;
           wHi: in std_logic_vector(31 downto 0);
           wLo: in std_logic_vector(31 downto 0);
           rHi: out std_logic_vector(31 downto 0);
           rLo: out std_logic_vector(31 downto 0));
end HiLoRegFile;


architecture BehavHiLoRegFile of HiLoRegFile is
    signal hi, lo: std_logic_vector(31 downto 0);
begin
    -- forwarding
    process (wE, wHi, wLo, hi, lo)
    begin
        if (wE = '1') then 
            rHi <= wHi;
            rLo <= wLo;
        else
            rHi <= hi;
            rLo <= lo;
        end if;
    end process;

    process (clk, rst)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                hi <= Zero32;
                lo <= Zero32;
            elsif (wE = '1') then             
                hi <= wHi;
                lo <= wLo;
            end if;
        end if;
    end process;

end BehavHiLoRegFile;
