----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 16:12:59
-- Design Name: 
-- Module Name: MEM - BehavMEM
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MEM is
    Port ( rst : in STD_LOGIC;
           -- GPR 
           wd : in STD_LOGIC_VECTOR (4 downto 0);
           wreg : in STD_LOGIC;
           wdata : in STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           wE_hilo: in std_logic; 
           hi_data: in std_logic_vector(31 downto 0);
           lo_data: in std_logic_vector(31 downto 0);
           -- CP0
           cp0reg_we: in std_logic;
           cp0reg_waddr: in std_logic_vector(4 downto 0);
           cp0reg_wdata: in std_logic_vector(31 downto 0);
           -- GPR 
           wd_o : out STD_LOGIC_VECTOR (4 downto 0);
           wreg_o : out STD_LOGIC;
           wdata_o : out STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           wE_hilo_o: out std_logic; 
           hi_data_o: out std_logic_vector(31 downto 0);
           lo_data_o: out std_logic_vector(31 downto 0);
           -- CP0
           cp0reg_we_o: out std_logic;
           cp0reg_waddr_o: out std_logic_vector(4 downto 0);
           cp0reg_wdata_o: out std_logic_vector(31 downto 0);
           -- RAM
           ramMode: in T_RAMMode;
           ramRData: in DWord;
           -- TLB
           TLBmiss: in std_logic;
           TLBwe: in std_logic;
           TLBrandom: in std_logic;
           TLBwe_o: out std_logic;
           TLBrandom_o: out std_logic;
           -- exceptions
           exceptType: in std_logic_vector(31 downto 0);
           isInDelaySlot: in std_logic;
           currentInstAddr: in std_logic_vector(31 downto 0);
           
           cp0Status_i: in std_logic_vector(31 downto 0);
           cp0Cause_i: in std_logic_vector(31 downto 0);
           cp0Epc_i: in std_logic_vector(31 downto 0);
           
           wbCp0RegWe: in std_logic;
           wbCp0RegWaddr: in std_logic_vector(4 downto 0);
           wbCp0RegWdata: in std_logic_vector(31 downto 0);
           
           exceptType_o: out std_logic_vector(31 downto 0);
           cp0Epc_o: out std_logic_vector(31 downto 0);
           isInDelaySlot_o: out std_logic;
           currentInstAddr_o: out std_logic_vector(31 downto 0)
       );
end MEM;

architecture BehavMEM of MEM is
    signal LLbit: std_logic;
    signal cp0Status: std_logic_vector(31 downto 0);
    signal cp0Cause: std_logic_vector(31 downto 0);
    signal cp0Epc: std_logic_vector(31 downto 0);
    signal MEMWe: std_logic;
    signal ExceptType_o1: std_logic_vector(31 downto 0);
begin
	isInDelaySlot_o <= isInDelaySlot;
	currentInstAddr_o <= currentInstAddr;
	ExceptType_o <= ExceptType_o1;
	TLBwe_o <= TLBwe;
	TLBrandom_o <= TLBrandom;
	process (all)
	begin
	   if (rst = RstEnable) then
	       cp0Status <= Zero32;
	   elsif ((wbCp0RegWe = WriteEnable) and (wbCp0RegWaddr = CP0Reg_status)) then
	       cp0Status <= wbCp0RegWdata;
	   else
	       cp0Status <= cp0Status_i;
	   end if;
	end process;
	
    process (all)
    begin
       if (rst = RstEnable) then
           cp0Epc  <= Zero32;
       elsif ((wbCp0RegWe = WriteEnable) and (wbCp0RegWaddr = CP0Reg_epc)) then
           cp0Epc <= wbCp0RegWdata;
       else
           cp0Epc <= cp0Epc_i;
       end if;
    end process;
    cp0Epc_o <= cp0Epc;
    
    process (all)
    begin
       -- latch avoid
       cp0Cause <= Zero32;
       if (rst = RstEnable) then
           cp0Cause <= Zero32;
       elsif ((wbCp0RegWe = WriteEnable) and (wbCp0RegWaddr = CP0Reg_Cause)) then
           cp0Cause(9 downto 8) <= wbCp0RegWdata(9 downto 8);
           cp0Cause(22) <= wbCp0RegWdata(22);
           cp0Cause(23) <= wbCp0RegWdata(23);
       else
           cp0Cause <= cp0Cause_i;
       end if;
    end process;
    
    process (all)
    begin
        if (rst = RstEnable) then
            exceptType_o1 <= Zero32;
        else   
            exceptType_o1 <= Zero32;
            if (not(currentInstAddr = Zero32)) then
                if ((not ((cp0Cause(15 downto 8) and cp0Status(15 downto 8)) = x"00"))
                and (cp0Status(1) = '0') and (cp0Status(0) = '1')) then
                    exceptType_o1 <= Except_Interrupt;
                elsif (exceptType(8) = '1') then
                    exceptType_o1 <= Except_Syscall;
                elsif (exceptType(9) = '1') then
                    exceptType_o1 <= Except_InstInvalid;
                elsif (exceptType(12) = '1') then
                    exceptType_o1 <= Except_Eret;
                elsif (TLBmiss = '1') then
                    if(ramMode = RAMMODE_WRITE_WORD or ramMode = RAMMODE_WRITE_BYTE) then
                        exceptType_o1 <= Except_TLBstore;
                    else 
                        exceptType_o1 <= Except_TLBload;
                    end if;
                end if;
            end if; 
        end if;
    end process;
    /* signal MEMWE_O hasn't been defined previously,
     * however, I can't find its usage.
     * I just adjust the code due to my own comprehension and may cause problems
     * (better not) 
     * if this unfortunately causes problem, delete MEMWe. 
     */
    MEMWe  <= not(or(ExceptType_o1));
	process (all)
	begin
		if (rst = RstEnable) then
			wd_o <= (others => '0');
			wreg_o <= WriteDisable;
			wdata_o <= Zero32;
            wE_hilo_o <= WriteDisable;
            hi_data_o <= Zero32;
            lo_data_o <= Zero32;
            cp0reg_we_o <= WriteDisable;
            cp0reg_waddr_o <= "00000";
            cp0reg_wdata_o <= Zero32;
		else
			wd_o <= wd and MEMWe;
			wreg_o <= wreg;
            if ((ramMode = RAMMODE_READ_BYTE) 
                    or (ramMode = RAMMODE_READ_WORD) 
                    or (ramMode = RAMMODE_READ_BYTEU)) then
                wdata_o <= ramRData;
            else
                wdata_o <= wdata;
            end if;
            wE_hilo_o <= wE_hilo and MEMWe;
            hi_data_o <= hi_data;
            lo_data_o <= lo_data;
            cp0reg_we_o <= cp0reg_we and MEMWe;
            cp0reg_waddr_o <= cp0reg_waddr;
            cp0reg_wdata_o <= cp0reg_wdata;
		end if;
	end process;

end BehavMEM;
