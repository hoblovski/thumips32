----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 15:59:34
-- Design Name: 
-- Module Name: EX - BehavEX
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EX is
    Port ( rst : in STD_LOGIC;
    	   inst: in STD_LOGIC_VECTOR(31 downto 0);
           ALUselect : in STD_LOGIC_VECTOR (2 downto 0);
           ALUop : in STD_LOGIC_VECTOR (7 downto 0);
           aluOp_ps: out std_logic_vector(7 downto 0);
           num1 : in STD_LOGIC_VECTOR (31 downto 0);
           num2 : in STD_LOGIC_VECTOR (31 downto 0);
           wd : in STD_LOGIC_VECTOR (4 downto 0);
           wreg : in STD_LOGIC;
           wd_o : out STD_LOGIC_VECTOR (4 downto 0);
           wreg_o : out STD_LOGIC;
           wdata_o : out STD_LOGIC_VECTOR (31 downto 0); -- computed result
           -- value from Hi/LO RF
           hi_rf: in std_logic_vector(31 downto 0);
           lo_rf: in std_logic_vector(31 downto 0);
           -- forwarding HI / LO (WB forwarding done in HILO_RF)
           MEM_fw_hi: in std_logic_vector(31 downto 0);
           MEM_fw_lo: in std_logic_vector(31 downto 0);
           MEM_fw_wE_hilo: in std_logic;
           -- write enable of HILORF for this inst
           wE_hilo: out std_logic; 
           hi_data: out std_logic_vector(31 downto 0);
           lo_data: out std_logic_vector(31 downto 0);
           -- forwarding CP0 registers
           MEM_cp0_reg_we: in std_logic;
           MEM_cp0_reg_waddr: in std_logic_vector(4 downto 0);
           MEM_cp0_reg_wdata: in std_logic_vector(31 downto 0);
           -- write back to CP0 registers
           wb_cp0_reg_we: in std_logic;
           wb_cp0_reg_waddr: in std_logic_vector(4 downto 0);
           wb_cp0_reg_wdata: in std_logic_vector(31 downto 0);
           -- directly read from CP0 registers
           cp0_reg_rdata: in std_logic_vector(31 downto 0);
           cp0_reg_raddr: out std_logic_vector(4 downto 0);
		   -- transport in pipeline to write to CP0 registers
		   cp0_reg_we: out std_logic;
		   cp0_reg_waddr: out std_logic_vector(4 downto 0);
		   cp0_reg_wdata: out std_logic_vector(31 downto 0);
           -- branch
           isInDelaySlot : in std_logic;
           LinkAddr : in std_logic_vector(31 downto 0);
           -- RAM
           ramMode: in T_RAMMode;
           ramMode_ps: out T_RAMMode; -- passed
           RAMwdata: in DWord;
           RAMwdata_ps: out DWord; -- passed to next
           -- TLB
           TLBwe: in std_logic;
           TLBrandom: in std_logic;
           TLBwe_o: out std_logic;
           TLBrandom_o: out std_logic;
           -- Exception
           exceptType : in std_logic_vector(31 downto 0);
           currentInstAddr: in std_logic_vector(31 downto 0);
           exceptType_o: out std_logic_vector(31 downto 0);
           currentInstAddr_o: out std_logic_vector(31 downto 0);
           isInDelaySlot_o: out std_logic
       );
end EX;

architecture BehavEX of EX is
	signal logicout: std_logic_vector(31 downto 0);
	signal shiftres: std_logic_vector(31 downto 0);
    signal arithout: std_logic_vector(31 downto 0);
    signal moveout: std_logic_vector(31 downto 0);
    -- value of HI / LO, considering forwarding
    signal hi: std_logic_vector(31 downto 0); 
    signal lo: std_logic_vector(31 downto 0); 
    /* it seems that we needn't implement overflow and trap exception
    -- exceptions
    signal ovassert: std_logic; -- overflow exception
    
    signal num2_mux : std_logic_vector(31 downto 0);
    signal result_sum: std_logic_vector(31 downto 0);
    signal ov_sum: std_logic;
    */
begin
    wd_o <= wd;
    wreg_o <= wreg;
    ramMode_ps <= ramMode;
    RAMwdata_ps <= RAMwdata;
    aluOp_ps <= aluOp;
    exceptType_o <= exceptType(31 downto 12) 
                                & "00" & exceptType(9 downto 8) & "00000000";
    isInDelaySlot_o <= isInDelaySlot;
    currentInstAddr_o <= currentInstAddr;
    TLBwe_o <= TLBwe;
    TLBrandom_o <= TLBrandom;
    
    -- forwarded result -> hi / lo
    process (all)
    begin
        if (MEM_fw_wE_hilo = '1') then
            hi <= MEM_fw_hi;
            lo <= MEM_fw_lo;
        else
            hi <= hi_rf;
            lo <= lo_rf;
        end if;
    end process;

    -- set overflow indicator(but we don't need to implement overflow exception) 
  /*  process (all)
    begin
        if (ALUOp = ALUOp_sub) then
            num2_mux <= comp_code(num2);
        else
            num2_mux <= num2; 
        end if;
        result_sum <=  std_logic_vector(unsigned(num1) + unsigned(num2_mux));
        ov_sum <= (((not num1(31)) and (not num2_mux(31))) and result_sum(31))
                        or ((num1(31) and num2_mux(31)) and (not result_sum(31)));
        if (((ALUOp = ALUOp_add) or (ALUOp = ALUOp_sub)) 
            and (ov_sum = '1')) then
            
        end if;
    end process;*/

	process (all)
        variable mult_res: std_logic_vector(63 downto 0);
	begin
        -- put everything default here to avoid latches
        logicout <= Zero32;
        arithout <= Zero32;
        moveout <= Zero32;
        shiftres <= Zero32;

        cp0_reg_waddr <= "00000";
        cp0_reg_we <= WriteDisable;
        cp0_reg_wdata <= Zero32;
        wE_hilo <= '0';
        hi_data <= Zero32;
        lo_data <= Zero32;

        cp0_reg_raddr <= (others=> '0');

		if (rst = RstEnable) then
            -- do nothing
		else 
			case ALUop is
				-- Logic
				when ALUop_or => 
					logicout <= num1 or num2;
				when ALUop_and =>
					logicout <= num1 and num2;
				when ALUop_xor =>
					logicout <= num1 xor num2;
				when ALUop_nor =>
					logicout <= num1 nor num2;
				-- Shift
				when ALUop_sll =>   -- sll
					shiftres <= to_stdlogicvector(to_bitvector(num2) sll to_integer(unsigned(num1(4 downto 0))));
				when ALUop_srl =>	--srl
					shiftres <= to_stdlogicvector(to_bitvector(num2) srl to_integer(unsigned(num1(4 downto 0))));
				when ALUop_sra =>	--sra
					shiftres <= to_stdlogicvector(to_bitvector(num2) sra to_integer(unsigned(num1(4 downto 0))));
				-- Move
				when ALUOP_MFHI =>      -- mfhi rd
                    moveout <= hi;
                when ALUOP_MFLO =>      -- mflo rd
                    --report "i = 0x" & to_hstring(to_signed(i, 32));
                    --report "aluop: mflo";
                    --report "lo: " & integer'image(to_integer(unsigned(lo)));
                    moveout <= lo;
                when ALUOP_MTHI =>      -- mthi rs
                    wE_hilo <= '1';
                    hi_data <= num1;    -- num1 is rs
                    lo_data <= lo;
                    moveout <= Zero32;
                when ALUop_MTLO =>      -- mtlo rs
                    wE_hilo <= '1';
                    hi_data <= hi;
                    lo_data <= num1;    -- num1 is rs
                    moveout <= Zero32;
                when ALUop_MFC0 =>
                	cp0_reg_raddr <= inst(15 downto 11);
                	moveout <= cp0_reg_rdata;
             		
                	if ((MEM_cp0_reg_we = WriteEnable) and (MEM_cp0_reg_waddr = inst(15 downto 11))) then
                		moveout <= MEM_cp0_reg_wdata;
                	else
                		if ((wb_cp0_reg_we = WriteEnable) and (wb_cp0_reg_waddr = inst(15 downto 11))) then
                			moveout <= wb_cp0_reg_wdata;
                		end if;
                	end if;
                when ALUop_MTC0 =>
					cp0_reg_waddr <= inst(15 downto 11);
					cp0_reg_we <= WriteEnable;
					cp0_reg_wdata <= num1;
				-- Arithmetic
                when ALUop_add =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_SW =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_SB =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_LW =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_LB =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_LBU =>
                    arithout <= std_logic_vector(unsigned(num1) + unsigned(num2));
                when ALUOP_SUB =>
                    arithout <= std_logic_vector(unsigned(num1) - unsigned(num2));
				when ALUop_cmp =>
                    if (signed(num1) < signed(num2)) then
                        arithout <= (0=> '1', others=> '0');
                    else
                        arithout <= (others=> '0');
                    end if;
				when ALUop_cmpu =>
                    if (unsigned(num1) < unsigned(num2)) then
                        arithout <= (0=> '1', others=> '0');
                    else
                        arithout <= (others=> '0');
                    end if;
                when ALUOP_MULT =>
                    mult_res := std_logic_vector(
                            signed(num1) * signed(num2));
                    wE_hilo <= '1';
                    hi_data <= mult_res(63 downto 32);
                    lo_data <= mult_res(31 downto 0);
				-- Branch/Jump
						
				-- Load/Store
						
				-- Others
				when others =>   -- invalid
					logicout <= Zero32;
                    arithout <= Zero32;
			end case;
		end if;
	end process;
	
	process (all)
	begin
		case ALUselect is
			when ALUSelect_logic => 
				wdata_o <= logicout; 
			when ALUselect_shift =>
			    wdata_o <= shiftres;
            when ALUSelect_arith =>
                wdata_o <= arithout;
            when ALUSelect_move =>
                wdata_o <= moveout;
            when ALUSelect_branchjump =>
            	wdata_o <= LinkAddr;
			when others =>          -- invalid
				wdata_o <= Zero32;
		end case;
	end process;

end BehavEX;
