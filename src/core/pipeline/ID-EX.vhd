----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 15:49:06
-- Design Name: 
-- Module Name: ID-EX - BehavIDEX
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ID_EX is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           IDALUselect : in STD_LOGIC_VECTOR (2 downto 0);
           IDALUop : in STD_LOGIC_VECTOR (7 downto 0);
           IDnum1 : in STD_LOGIC_VECTOR (31 downto 0);
           IDnum2 : in STD_LOGIC_VECTOR (31 downto 0);
           IDwd : in STD_LOGIC_VECTOR (4 downto 0);
           IDwreg : in STD_LOGIC;
           IDLinkAddr : in STD_LOGIC_VECTOR(31 downto 0);
           IDInDelaySlot : in STD_LOGIC;
           IDNextInDelaySlot : in STD_LOGIC;
           IDinst: in STD_LOGIC_VECTOR(31 downto 0);
           IDTLBwe: in std_logic;
           IDTLBrandom: in std_logic;
           EXLinkAddr : out STD_LOGIC_VECTOR(31 downto 0);
           EXInDelaySlot : out STD_LOGIC;
           isInDelaySlot : out STD_LOGIC;
           EXALUselect : out STD_LOGIC_VECTOR (2 downto 0);
           EXALUop : out STD_LOGIC_VECTOR (7 downto 0);
           EXnum1 : out STD_LOGIC_VECTOR (31 downto 0);
           EXnum2 : out STD_LOGIC_VECTOR (31 downto 0);
           EXwd : out STD_LOGIC_VECTOR (4 downto 0);
           EXwreg : out STD_LOGIC;
           EXinst: out STD_LOGIC_VECTOR(31 downto 0);
           EXTLBwe: out std_logic;
           EXTLBrandom: out std_logic; 
           -- RAM
           IDramMode: in T_RAMMode;
           IDRAMwdata: in DWord; 
           EXramMode: out T_RAMMode;
           EXramWData: out DWord;
           -- Exception
           flush: in STD_LOGIC;
           IDCurrentInstAddr: in STD_LOGIC_VECTOR(31 downto 0);
           IDExceptType: in STD_LOGIC_VECTOR(31 downto 0);
           EXCurrentInstAddr: out STD_LOGIC_VECTOR(31 downto 0);
           EXExceptType: out STD_LOGIC_VECTOR(31 downto 0);
           -- stalling 
           IDstall: in std_logic;   -- 0 if not stall
           IFstall: in std_logic;
           MEMstall: in std_logic
       );
end ID_EX;

architecture BehavIDEX of ID_EX is
begin
	
	process(clk, rst) 
        variable stall: std_logic;
	begin
		if (rising_edge(clk)) then
            stall := IDstall or MEMstall or IFstall;
			if (rst = RstEnable) then
				EXALUselect <= ALUselect_nop;
				EXALUop <= ALUop_nop;
				EXnum1 <= Zero32;
				EXnum2 <= Zero32;
				EXwd <= (others => '0');
				EXwreg <= WriteDisable;
				EXLinkAddr <= Zero32;
				EXInDelaySlot <= NotInDelaySlot;
				isInDelaySlot <= NotInDelaySlot;
				EXinst <= Zero32;
                EXramMode <= RAMMODE_NOP;
                EXramWData <= Zero32;
                EXExceptType <= Zero32;
                EXCurrentInstAddr <= Zero32; 
                EXTLBwe <= '0';
                EXTLBrandom <= '0';
			elsif (flush = '1') then
			    EXALUop <= ALUop_nop;
			    EXALUselect <= ALUselect_nop;
			    EXnum1 <= Zero32;
			    EXnum2 <= Zero32;
			    EXwd <= (others => '0');
			    EXwreg <= WriteDisable;
			    EXExceptType <= Zero32;
			    EXLinkAddr <= Zero32;
			    EXInst <= Zero32;
			    EXInDelaySlot <= NotInDelaySlot;
			    IsInDelaySLot <= NotInDelaySlot;
			    EXCurrentInstAddr <= Zero32;
                EXramMode <= RAMMODE_NOP;
                EXramWData <= Zero32;
                EXTLBwe <= '0';
                EXTLBrandom <= '0';
            elsif (stall = '0') then
				EXALUselect <= IDALUselect;
				EXALUop <= IDALUop;
				EXnum1 <= IDnum1;
				EXnum2 <= IDnum2;
				EXwd <= IDwd;
				EXwreg <= IDwreg;
				EXLinkAddr <= IDLinkAddr;
				EXInDelaySlot <= IDInDelaySlot;
				isInDelaySlot <= IDNextInDelaySlot;
				EXinst <= IDinst;
                EXramMode <= IDramMode;
                EXramWData <= IDRAMwdata;
                EXExceptType <= IDExceptType;
                EXCurrentInstAddr <= IDCurrentInstAddr;
                EXTLBwe <= IDTLBwe;
                EXTLBrandom <= IDTLBrandom;
            elsif (stall = '1' and MEMstall = '0') then
                -- insert a nop inst
			    EXALUop <= ALUop_nop;
			    EXALUselect <= ALUselect_nop;
			    EXnum1 <= Zero32;
			    EXnum2 <= Zero32;
			    EXwd <= (others => '0');
			    EXwreg <= WriteDisable;
			    EXExceptType <= Zero32;
			    EXLinkAddr <= Zero32;
			    EXInst <= Zero32;
			    EXInDelaySlot <= NotInDelaySlot;
			    IsInDelaySLot <= NotInDelaySlot;
			    EXCurrentInstAddr <= Zero32;
                EXramMode <= RAMMODE_NOP;
                EXramWData <= Zero32;
                EXTLBwe <= '0';
                EXTLBrandom <= '0';
            end if;
		end if;
	end process;

end BehavIDEX;
