----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 11:16:20
-- Design Name: 
-- Module Name: IF_ID - BehavIFID
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IF_ID is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           IFpc : in STD_LOGIC_VECTOR (31 downto 0);
           IFinst : in STD_LOGIC_VECTOR (31 downto 0);
           flush : in STD_LOGIC;
           IDpc : out STD_LOGIC_VECTOR (31 downto 0);
           IDinst : out STD_LOGIC_VECTOR (31 downto 0);
           -- control signals 
           -- stalling 
           IDstall: in std_logic;   -- 0 if not stall
           IFstall: in std_logic;
           MEMstall: in std_logic
    );
end IF_ID;

architecture BehavIFID of IF_ID is
begin
	process (clk, rst)
        variable stall: std_logic;
	begin
		if (rising_edge(clk)) then
            stall := IFstall or IDstall or MEMstall;
			if (rst = RstEnable) then
				IDpc <= Zero32;
				IDinst <= Zero32;
			elsif (flush = '1') then
				IDpc <= Zero32;
				IDinst <= Zero32;
            elsif (stall = '0') then
                -- CPU not stall, IF results go into ID
				IDpc <= IFpc;
				IDinst <= IFinst;
--            elsif (stall = '1' and IDstall = '0' and MEMstall = '0') then
--                IDpc <= Zero32;
  --              IDinst <= Zero32;
            end if;
            -- else: cpu stalls, no data should go from IF into ID
		end if;
	end process;
end BehavIFID;
