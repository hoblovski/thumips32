----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 16:08:01
-- Design Name: 
-- Module Name: EX_MEM - BehavEXMEM
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EX_MEM is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           -- GPR 
           EXwd : in STD_LOGIC_VECTOR (4 downto 0);
           EXwreg : in STD_LOGIC;
           EXwdata : in STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           EXwE_hilo: in std_logic; 
           EXhi_data: in std_logic_vector(31 downto 0);
           EXlo_data: in std_logic_vector(31 downto 0);
           -- CP0
           EXcp0_reg_we: in std_logic;
           EXcp0_reg_waddr: in std_logic_vector(4 downto 0);
           EXcp0_reg_wdata: in std_logic_vector(31 downto 0);
           -- TLB
           EXTLBwe: in std_logic;
           EXTLBrandom: in std_logic;
           -- GPR
           MEMwd : out STD_LOGIC_VECTOR (4 downto 0);
           MEMwreg : out STD_LOGIC;
           MEMwdata : out STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           MEMwE_hilo: out std_logic; 
           MEMhi_data: out std_logic_vector(31 downto 0);
           MEMlo_data: out std_logic_vector(31 downto 0);
           -- CP0
           MEMcp0_reg_we: out std_logic;
           MEMcp0_reg_waddr: out std_logic_vector(4 downto 0);
           MEMcp0_reg_wdata: out std_logic_vector(31 downto 0);
           -- TLB
           MEMTLBwe: out std_logic;
           MEMTLBrandom: out std_logic;
           -- RAM
           EXramMode: in T_RAMMode;
           EXramWData: in DWord;
           MEMramMode: out T_RAMMode;
           MEMramWData: out DWord;
           -- RAMaddr is computed result of EX -- EXwdata
           -- Exceptions
           flush: in std_logic;
           EXExceptType: in std_logic_vector(31 downto 0);
           EXisInDelaySlot: in std_logic;
           EXCurrentInstAddr: in std_logic_vector(31 downto 0);
           MEMExceptType: out std_logic_vector(31 downto 0);
           MEMisInDelaySlot: out std_logic;
           MEMCurrentInstAddr: out std_logic_vector(31 downto 0);
           -- stalling 
           IDstall: in std_logic;   -- 0 if not stall
           IFstall: in std_logic;
           MEMstall: in std_logic
       );
end EX_MEM;

architecture BehavEXMEM of EX_MEM is

begin
	process (clk, rst)
        variable stall: std_logic;
	begin
		if (rising_edge(clk)) then
            stall := MEMstall;
			if (rst = RstEnable) then
				MEMwd <= (others => '0');
				MEMwreg <= WriteDisable;
				MEMwdata <= Zero32;
                MEMwE_hilo <= WriteDisable;
                MEMhi_data <= Zero32;
                MEMlo_data <= Zero32;
                MEMcp0_reg_we <= WriteDisable;
                MEMcp0_reg_waddr <= "00000";
                MEMcp0_reg_wdata <= Zero32;
                MEMramMode <= RAMMODE_NOP;
                MEMramWData <= Zero32;
                MEMExceptType <= Zero32;
                MEMisInDelaySlot <= NotInDelaySlot;
                MEMCurrentInstAddr <= Zero32;
                MEMTLBwe <= '0';
                MEMTLBrandom <= '0';
			elsif (flush = '1') then 
			    MEMwd <= (others => '0');
                MEMwreg <= WriteDisable;
                MEMwdata <= Zero32;
                MEMwE_hilo <= WriteDisable;
                MEMhi_data <= Zero32;
                MEMlo_data <= Zero32;
                MEMcp0_reg_we <= WriteDisable;
                MEMcp0_reg_waddr <= "00000";
                MEMcp0_reg_wdata <= Zero32;
                MEMramMode <= RAMMODE_NOP;
                MEMramWData <= Zero32;
                MEMExceptType <= Zero32;
                MEMisInDelaySlot <= NotInDelaySlot;
                MEMCurrentInstAddr <= Zero32;
                MEMTLBwe <= '0';
                MEMTLBrandom <= '0';
            elsif(stall = '0') then
				MEMwd <= EXwd;
				MEMwreg <= EXwreg;
				MEMwdata <= EXwdata;
                MEMwE_hilo <= EXwE_hilo;
                MEMhi_data <= EXhi_data;
                MEMlo_data <= EXlo_data;
				MEMcp0_reg_we <= EXcp0_reg_we;
				MEMcp0_reg_waddr <= EXcp0_reg_waddr;
				MEMcp0_reg_wdata <= EXcp0_reg_wdata;
                MEMramMode <= EXramMode;
                MEMramWData <= EXramWData;
                MEMExceptType <= EXExceptType;
                MEMisInDelaySlot <= EXisInDelaySlot;
                MEMCurrentInstAddr <= EXCurrentInstAddr;
                MEMTLBwe <= EXTLBwe;
                MEMTLBrandom <= EXTLBrandom;
			end if;
		end if;
	end process;

end BehavEXMEM;
