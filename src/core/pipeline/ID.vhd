----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/10/14 14:56:31
-- Design Name:
-- Module Name: ID - BehavID
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ID is
    Port ( rst : in STD_LOGIC;
           pc : in STD_LOGIC_VECTOR (31 downto 0);
           inst : in STD_LOGIC_VECTOR (31 downto 0);
           -- GPR interface
           reg1_data : in STD_LOGIC_VECTOR (31 downto 0);
           reg2_data : in STD_LOGIC_VECTOR (31 downto 0);
           reg1_read_o : out STD_LOGIC;
           reg2_read_o : out STD_LOGIC;
           reg1_addr_o : out STD_LOGIC_VECTOR (4 downto 0);
           reg2_addr_o : out STD_LOGIC_VECTOR (4 downto 0);
           -- ALU interface
           ALUop : out STD_LOGIC_VECTOR (7 downto 0);
           ALUselect : out STD_LOGIC_VECTOR (2 downto 0);
           num1_o : out STD_LOGIC_VECTOR (31 downto 0);
           num2_o : out STD_LOGIC_VECTOR (31 downto 0);
           -- WB intreface
           wd : out STD_LOGIC_VECTOR (4 downto 0);
           wreg : out STD_LOGIC;
           -- forwarding
           EXwreg: in std_logic;
           EXwdata: in std_logic_vector(31 downto 0);
           EXwd: in std_logic_vector(4 downto 0);
           MEMwreg: in std_logic;
           MEMwdata: in std_logic_vector(31 downto 0);
           MEMwd: in std_logic_vector(4 downto 0);
           -- branch
           isInDelaySlot: in std_logic;
           isInDelaySlot_o: out std_logic;
           NextInDelaySlot: out std_logic;
           BranchFlag: out std_logic;
           BranchAddr: out std_logic_vector(31 downto 0);
           LinkAddr: out std_logic_vector(31 downto 0);
           -- Memory interface
           ramMode: out T_RAMMode;
           RAMwdata: out DWord;
           -- TLB
           TLBwe: out std_logic;
           TLBrandom: out std_logic;
           -- Data hazard stalling
           EXaluOp: in AluOpCode; -- check load insts.
           IDstall: out std_logic; -- 0 if not stall
           -- Exceptions 
           exceptType: out std_logic_vector(31 downto 0);
           currentInstAddr: out std_logic_vector(31 downto 0);
           
           pcMoveImm_SignedExtendTEST: out Dword;
           pcPlus4TEST: out Dword
           );
end ID;

architecture BehavID of ID is
	signal op: std_logic_vector(5 downto 0);
	signal op1:std_logic_vector(4 downto 0);
	signal op2:std_logic_vector(5 downto 0);
	signal op3:std_logic_vector(4 downto 0);
	signal opC0: std_logic_vector(4 downto 0);

    signal rsval: DWord;    -- value of R[rs] after forwarding
    signal rtval: DWord;    -- value of R[rt] after forwarding
	signal immediate: std_logic_vector(31 downto 0);

	signal instval: std_logic;
	signal reg1_read, reg2_read: std_logic;
	signal reg1_addr, reg2_addr: std_logic_vector(4 downto 0);
	signal num1, num2: std_logic_vector(31 downto 0);

	signal pcPLUS8: std_logic_vector(31 downto 0);
	signal pcPLUS4: std_logic_vector(31 downto 0);
	signal pcMoveImm_SignedExtend : std_logic_vector(31 downto 0);

    signal exceptTypeIsSyscall: std_logic;
    signal exceptTypeIsEret: std_logic;
    
begin
	reg1_read_o <= reg1_read;
	reg2_read_o <= reg2_read;
	reg1_addr_o <= reg1_addr;
	reg2_addr_o <= reg2_addr;
	num1_o <= num1;
	num2_o <= num2;

	op  <= inst(31 downto 26);
	op1 <= inst(10 downto 6);
	op2 <= inst(5 downto 0);
	op3 <= inst(20 downto 16);
	opC0 <= inst(25 downto 21);

	pcPLUS8 <= std_logic_vector(unsigned(pc) + 8);
	pcPLUS4 <= std_logic_vector(unsigned(pc) + 4);
	pcMoveImm_SignedExtend <= sign_extend(inst(15 downto 0))(29 downto 0) & "00";
	pcMoveImm_SignedExtendTEST <= pcMoveImm_SignedExtend;
	pcPlus4TEST <= pcPLUS4;

    exceptType <= "0000000000000000000" & exceptTypeIsEret 
                           & "00" & instval & exceptTypeIsSyscall 
                           & "00000000";
    currentInstAddr <= pc;

    process (all)
        variable isExInstLoad: boolean;
    begin
        isExInstLoad := ((EXaluOp = ALUOP_LW) 
                or (EXaluOp = ALUOP_LB) or (EXaluOp = ALUOP_LBU));
        if (isExInstLoad and 
                (((EXwd = reg1_addr) and (reg1_read = ReadEnable))
                or ((EXwd = reg2_addr) and (reg2_read = ReadEnable)))) then
            IDstall <= '1';
        else
            IDstall <= '0';
        end if;
    end process;

	process (all)
        variable t: std_logic_vector(15 downto 0);
	begin
        -- avoid latches
        exceptTypeIsEret <= FalseLogic;
		if (rst = RstEnable) then
			ALUop <= ALUop_nop;
			ALUselect <= ALUselect_nop;
			wd <= "00000";
			wreg <= WriteDisable;
			instval <= InstValid;
			reg1_read <= ReadDisable;
			reg2_read <= ReadDisable;
			reg1_addr <= "00000";
			reg2_addr <= "00000";
			immediate <= Zero32;
			LinkAddr <= Zero32;
			BranchAddr <= Zero32;
			BranchFlag <= NotBranch;
			NextInDelaySlot <= NotInDelaySlot;
            ramMode <= RAMMODE_NOP;
            RAMwdata <= Zero32;
            TLBwe <= '0';
            TLBrandom <= '0';
		else
			ALUop <= ALUop_nop;
			ALUselect <= ALUselect_nop;
			wd <= inst(15 downto 11);
			wreg <= WriteDisable;
			instval <= InstInvalid;
			reg1_read <= ReadDisable;
			reg2_read <= ReadDisable;
			reg1_addr <= inst(25 downto 21);
			reg2_addr <= inst(20 downto 16);
			immediate <= Zero32;
			LinkAddr <= Zero32;
			BranchAddr <= Zero32;
			BranchFlag <= NotBranch;
			NextInDelaySlot <= NotInDelaySlot;
            ramMode <= RAMMODE_NOP;
            RAMwdata <= Zero32;
            exceptTypeIsSyscall <= FalseLogic;
            exceptTypeIsEret <= FalseLogic;
            TLBwe <= '0';
            TLBrandom <= '0';
			case op is
				-- Logic
				when Op_ori =>
					wreg <= WriteEnable;
					ALUop <= ALUop_or;
					ALUselect <= ALUselect_logic;
					reg1_read <= ReadEnable;
					reg2_read <= ReadDisable;
					immediate <= Zero16 & inst(15 downto 0);
					wd <= inst(20 downto 16);
					instval <= InstValid;
				when Op_andi =>
					wreg <= WriteEnable;
					ALUop <= ALUop_and;
					ALUselect <= ALUselect_logic;
					reg1_read <= ReadEnable;
					reg2_read <= ReadDisable;
					immediate <= Zero16 & inst(15 downto 0);
					wd <= inst(20 downto 16);
					instval <= InstValid;
				when Op_xori =>
					wreg <= WriteEnable;
					ALUop <= ALUop_xor;
					ALUselect <= ALUselect_logic;
					reg1_read <= ReadEnable;
					reg2_read <= ReadDisable;
					immediate <= Zero16 & inst(15 downto 0);
					wd <= inst(20 downto 16);
					instval <= InstValid;
				when Op_lui =>
					wreg <= WriteEnable;
					ALUop <= ALUop_or;
					ALUselect <= ALUselect_logic;
					reg1_read <= ReadEnable;
					reg2_read <= ReadDisable;
					immediate <= inst(15 downto 0) & Zero16;
					wd <= inst(20 downto 16);
					instval <= InstValid;
				-- Arithmetic
                when Op_addiu =>
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    ALUop <= ALUop_add;
                    ALUselect <= ALUselect_arith;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    instval <= InstValid;
                when Op_slti =>
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    ALUop <= ALUop_cmp;
                    ALUselect <= ALUselect_arith;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    instval <= InstValid;
                when Op_sltiu =>
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    ALUop <= ALUop_cmpu;
                    ALUselect <= ALUselect_arith;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    instval <= InstValid;
                -- Branch/Jump
                when Op_j =>
                	wreg <= WriteDisable;
                	ALUop <= ALUop_j;
                	ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadDisable;
                	reg2_read <= ReadDisable;
                	BranchAddr <= pcPLUS4(31 downto 28) & inst(25 downto 0) & "00";
                	LinkAddr <= Zero32;
                	BranchFlag <= Branch;
                	NextInDelaySlot <= InDelaySlot;
                	instval <= InstValid;
                when Op_jal =>
                	wreg <= WriteEnable;
                	ALUop <= ALUop_jal;
                	ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadDisable;
                	reg2_read <= ReadDisable;
                	wd <= "11111";
                	BranchAddr <= pcPLUS4(31 downto 28) & inst(25 downto 0) & "00";
                	LinkAddr <= pcPLUS8;
                	BranchFlag <= Branch;
                	NextInDelaySlot <= InDelaySlot;
                	instval <= InstValid;
                when Op_beq =>
                	wreg <= WriteDisable;
                	ALUop <= ALUop_beq;
                	ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadEnable;
                	reg2_read <= ReadEnable;
                	instval <= InstValid;
                	if (num1 = num2) then
                		BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                		BranchFlag <= Branch;
                		NextInDelaySlot <= InDelaySlot;
                	end if;
                when Op_bgtz =>
                	wreg <= WriteDisable;
                	ALUop <= ALUop_bgtz;
                	ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadEnable;
                	reg2_read <= ReadDisable;
                	instval <= InstValid;
                	if (num1 /= Zero32 and num1(31) = '0') then
                		BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                		BranchFlag <= Branch;
                		NextInDelaySlot <= InDelaySlot;
                	end if;
                when Op_blez =>
                	wreg <= WriteDisable;
                	ALUop <= ALUop_bltz;
                	ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadEnable;
                	reg2_read <= ReadDisable;
                	instval <= InstValid;
                	if (num1 = Zero32 or num1(31) = '1') then
                		BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                		BranchFlag <= Branch;
                		NextInDelaySlot <= InDelaySlot;
                	end if;
                when Op_bne =>
                	wreg <= WriteDisable;
                	ALUop <= ALUop_bne;
               		ALUselect <= ALUselect_branchjump;
                	reg1_read <= ReadEnable;
                	reg2_read <= ReadEnable;
                	instval <= InstValid;
                	if(num1 /= num2) then
                		BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                		BranchFlag <= Branch;
                		NextInDelaySlot <= InDelaySlot;
                	end if;

                -- Load/Store
                when OP_SW =>
                    wreg <= WriteDisable;
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    ALUop <= ALUOP_SW;
                    ALUselect <= ALUSELECT_ARITH;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    ramMode <= RAMMODE_WRITE_WORD;
                    RAMwdata <= rtval;
                    instval <= InstValid;
                when OP_SB =>
                    wreg <= WriteDisable;
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    ALUop <= ALUOP_SB;
                    ALUselect <= ALUSELECT_ARITH;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    ramMode <= RAMMODE_WRITE_BYTE;
                    RAMwdata <= rtval;
                    instval <= InstValid;
                when OP_LW =>
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    aluOp <= ALUOP_LW;
                    aluSelect <= ALUSELECT_ARITH;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    ramMode <= RAMMODE_READ_WORD;
                    instval <= InstValid;
                when OP_LB =>
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    aluOp <= ALUOP_LB;
                    aluSelect <= ALUSELECT_ARITH;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    ramMode <= RAMMODE_READ_BYTE;
                    instval <= InstValid;
                when OP_LBU =>
                    wreg <= WriteEnable;
                    wd <= inst(RANGE_RT);
                    reg1_read <= ReadEnable;
                    reg2_read <= ReadDisable;
                    aluOp <= ALUOP_LBU;
                    aluSelect <= ALUSELECT_ARITH;
                    immediate <= sign_extend(inst(RANGE_IMM));
                    ramMode <= RAMMODE_READ_BYTEU;
                    instval <= InstValid;
                -- Others
                when OP_C0 =>
                	if (Op1 = Op1_C0 and (Op2 = Op2_C0 or Op2 = "000001")) then
                		case (OpC0) is
                			when OpC0_MFC0 =>
                				ALUop <= ALUop_MFC0;
                				ALUselect <= ALUselect_move;
                				wd <= inst(20 downto 16);
                				wreg <= WriteEnable;
                				instval <= InstValid;
                				reg1_read <= ReadDisable;
                				reg2_read <= ReadDisable;
                		 	when OpC0_MTC0 =>
                		 		ALUop <= ALUop_MTC0;
                		 		ALUselect <= ALUselect_nop;
                		 		wreg <= WriteDisable;
                		 		instval <= InstValid;
                		 		reg1_read <= ReadEnable;
                		 		reg1_addr <= inst(20 downto 16);
                		 		reg2_read <= ReadDisable;
                		 	when others =>
                		end case;
                	elsif (inst = Inst_eret) then
                	    wreg <= WriteDisable;
                	    ALUop <= ALUop_eret;
                	    ALUselect <= ALUselect_nop;
                	    reg1_read <= ReadDisable;
                	    reg2_read <= ReadDisable;
                	    instval <= InstValid;
                	    exceptTypeIsEret <= TrueLogic;
                    elsif (inst = Inst_tlbwi) then
                        wreg <= WriteDisable;
                        ALUop <= ALUop_nop;
                        ALUselect <= ALUselect_nop;
                        TLBwe <= '1';
                        TLBrandom <= '0';
                        reg1_read <= ReadDisable;
                        reg2_read <= ReadDisable;
                        instval <= InstValid;
                    elsif (inst = Inst_tlbwr) then
                        wreg <= WriteDisable;
                        ALUop <= ALUop_nop;
                        ALUselect <= ALUselect_nop;
                        TLBwe <= '1';
                        TLBrandom <= '1';
                        reg1_read <= ReadDisable;
                        reg2_read <= ReadDisable;
                        instval <= InstValid;
                	end if;
                -- Special
                when OP_SPECIAL =>
                    case op2 is
                    	-- Logic
                    	when Op2_or =>
                    		wreg <= WriteEnable;
                    		ALUop <= ALUop_or;
                    		ALUselect <=ALUselect_logic;
                    		reg1_read <= ReadEnable;
                    		reg2_read <= ReadEnable;
                    		instval <= InstValid;
                    	when Op2_and =>
                    		wreg <= WriteEnable;
                    		ALUop <= ALUop_and;
                    		ALUselect <=ALUselect_logic;
                    		reg1_read <= ReadEnable;
                    		reg2_read <= ReadEnable;
                    		instval <= InstValid;
                    	when Op2_xor =>
                    		wreg <= WriteEnable;
                    		ALUop <= ALUop_xor;
                    		ALUselect <=ALUselect_logic;
                    		reg1_read <= ReadEnable;
                    		reg2_read <= ReadEnable;
                    		instval <= InstValid;
                    	when Op2_nor =>
                    		wreg <= WriteEnable;
                    		ALUop <= ALUop_nor;
                    		ALUselect <=ALUselect_logic;
                    		reg1_read <= ReadEnable;
                    		reg2_read <= ReadEnable;
                    		instval <= InstValid;
                    	-- Arithmetic
                        when Op2_ADDU =>
                            reg1_read <= ReadEnable;
                            reg2_read <= ReadEnable;
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUop_add;
                            ALUselect <= ALUselect_arith;
                            instval <= InstValid;
                        when Op2_SUBU =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadEnable;
                            reg2_addr <= inst(RANGE_RT);
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUop_sub;
                            ALUselect <= ALUselect_arith;
                            instval <= InstValid;
                        when Op2_SLT =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadEnable;
                            reg2_addr <= inst(RANGE_RT);
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUop_cmp;
                            ALUselect <= ALUselect_arith;
                            instval <= InstValid;
                        when Op2_SLTU =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadEnable;
                            reg2_addr <= inst(RANGE_RT);
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUop_cmpu;
                            ALUselect <= ALUselect_arith;
                            instval <= InstValid;
                        when Op2_MULT =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadEnable;
                            reg2_addr <= inst(RANGE_RT);
                            wreg <= WriteDisable;
                            ALUop <= ALUOP_MULT;
                            ALUselect <= ALUSELECT_ARITH;
                            instval <= INSTVALID;
                        -- Move
                        when Op2_MFHI =>
                            reg1_read <= ReadDisable;
                            reg2_read <= ReadDisable;
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUOP_MFHI;
                            ALUselect <= ALUSELECT_MOVE;
                            instval <= INSTVALID;
                        when Op2_MFLO =>
                            reg1_read <= ReadDisable;
                            reg2_read <= ReadDisable;
                            wreg <= WriteEnable;
                            wd <= inst(RANGE_RD);
                            ALUop <= ALUOP_MFLO;
                            ALUselect <= ALUSELECT_MOVE;
                            instval <= INSTVALID;
                        when Op2_MTHI =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadDisable;
                            wreg <= WriteDisable;
                            ALUop <= ALUOP_MTHI;
                            ALUselect <= ALUSELECT_MOVE;
                            instval <= INSTVALID;
                        when Op2_MTLO =>
                            reg1_read <= ReadEnable;
                            reg1_addr <= inst(RANGE_RS);
                            reg2_read <= ReadDisable;
                            wreg <= WriteDisable;
                            ALUop <= ALUOP_MTLO;
                            ALUselect <= ALUSELECT_MOVE;
                            instval <= INSTVALID;
                        -- Shift
                        when Op2_sllv =>
                            wreg <= WriteEnable;
                            ALUop <= ALUop_sll;
                            ALUselect <= ALUselect_shift;
                            reg1_read <= ReadEnable;
                            reg2_read <= ReadEnable;
                            instval <= InstValid;
                        when Op2_srlv =>
                            wreg <= WriteEnable;
                            ALUop <= ALUop_srl;
                            ALUselect <= ALUselect_shift;
                            reg1_read <= ReadEnable;
                            reg2_read <= ReadEnable;
                            instval <= InstValid;
                        when Op2_srav =>
                            wreg <= WriteEnable;
                            ALUop <= ALUop_sra;
                            ALUselect <= ALUselect_shift;
                            reg1_read <= ReadEnable;
                            reg2_read <= ReadEnable;
                            instval <= InstValid;
                        -- Jump
                        when Op2_jr =>
                        	wreg <= WriteDisable;
                        	ALUop <= ALUOp_jr;
                        	ALUselect <= ALUselect_branchjump;
                        	reg1_read <= ReadEnable;
                        	reg2_read <= ReadDisable;
                        	LinkAddr <= Zero32;
                        	BranchAddr <= num1;
                        	BranchFlag <= Branch;
                        	NextInDelaySlot <= InDelaySlot;
                        	instval <= InstValid;
                        when Op2_jalr =>
                        	wreg <= WriteEnable;
                        	ALUop <= ALUOp_jalr;
                        	ALUselect <= ALUselect_branchjump;
                        	reg1_read <= ReadEnable;
                        	reg2_read <= ReadDisable;
                        	LinkAddr <= pcPLUS8;
                        	BranchAddr <= num1;
                        	BranchFlag <= Branch;
                        	NextInDelaySlot <= InDelaySlot;
                        	instval <= InstValid;
                        -- Syscall
                        when Op2_syscall =>
                            wreg <= WriteDisable;
                            ALUop <= ALUOp_syscall;
                            ALUselect <= ALUSelect_nop;
                            reg1_read <= ReadDisable;
                            reg2_read <= ReadDisable;
                            instval <= InstValid;
                            exceptTypeIsSyscall <= TrueLogic;
                        when others =>
                    end case;
                when Op_bgez_bltz =>
                	case op3 is
                		when Op3_bgez =>
                			wreg <= WriteDisable;
                			ALUop <= ALUop_bgez;
                			ALUselect <= ALUselect_branchjump;
                			reg1_read <= ReadEnable;
                			reg2_read <= ReadDisable;
                			instval <= InstValid;
                			if (num1(31) = '0') then
                				BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                				BranchFlag <= Branch;
                				NextInDelaySlot <= InDelaySlot;
                			end if;
                		when Op3_bltz =>
                			wreg <= WriteDisable;
                			ALUop <= ALUop_bgez;
                			ALUselect <= ALUselect_branchjump;
                			reg1_read <= ReadEnable;
                			reg2_read <= ReadDisable;
                			instval <= InstValid;
                			if (num1(31) = '1') then
                				BranchAddr <= std_logic_vector(unsigned(pcPLUS4) + unsigned(pcMoveImm_SignedExtend));
                				BranchFlag <= Branch;
                				NextInDelaySlot <= InDelaySlot;
                			end if;
                		when others =>
                	end case;
				when others => -- invalid op
			end case;

			-- Shift_part2: sll, srl, sra
			if (inst(31 downto 21) = "00000000000") then
			     case op2 is
				 -- sll
			         when Op2_sll =>
			             wreg <= WriteEnable;
			             ALUop <= ALUop_sll;
			             ALUselect <= ALUSelect_shift;
			             reg1_read <= ReadDisable;
			             reg2_read <= ReadEnable;
			             immediate <= Zero16 & "00000000000" & inst(10 downto 6);
			             wd <= inst(15 downto 11);
			             instval <= InstValid;
			     -- srl
			     	when Op2_srl =>
			     		wreg <= WriteEnable;
			     		ALUop <= ALUop_srl;
						ALUselect <= ALUSelect_shift;
						reg1_read <= ReadDisable;
						reg2_read <= ReadEnable;
						immediate <= Zero16 & "00000000000" & inst(10 downto 6);
			     		wd <= inst(15 downto 11);
			     		instval <= InstValid;
			     -- sra
					when Op2_sra =>
						wreg <= WriteEnable;
						ALUop <= ALUop_sra;
						ALUselect <= ALUSelect_shift;
						reg1_read <= ReadDisable;
						reg2_read <= ReadEnable;
						immediate <= Zero16 & "00000000000" & inst(10 downto 6);
						wd <= inst(15 downto 11);
						instval <= InstValid;
			         when others => --invalid op
			     end case;
		    end if;
		    
		end if;
	end process;

	process(rst, isInDelaySlot)
	begin
		if (rst = RstEnable) then
			isInDelaySlot_o <= NotInDelaySlot;
		else
			isInDelaySlot_o <= isInDelaySlot;
		end if;
	end process;

    -- compute rsval
    process (all)
    begin
        if ((EXwreg = WriteEnable) and (EXwd = reg1_addr)) then
			rsval <= EXwdata;
		elsif ((MEMwreg = WriteEnable) and (MEMwd = reg1_addr)) then
			rsval <= MEMwdata;
        else
			rsval <= reg1_data;
        end if;
    end process;

    -- compute rtval
    process (all)
    begin
        if ((EXwreg = WriteEnable) and (EXwd = reg2_addr)) then
			rtval <= EXwdata;
		elsif ((MEMwreg = WriteEnable) and (MEMwd = reg2_addr)) then
			rtval <= MEMwdata;
        else
			rtval <= reg2_data;
        end if;
    end process;

	process (all)
	begin
		if (rst = RstEnable) then
			num1 <= Zero32;
        elsif (reg1_read = ReadEnable) then
            num1 <= rsval;
        else
			num1 <= immediate;
		end if;
	end process;

	process (all)
	begin
		if (rst = RstEnable) then
			num2 <= Zero32;
        elsif (reg2_read = ReadEnable) then
            num2 <= rtval;
		else
			num2 <= immediate;
		end if;
	end process;

end BehavID;
