----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 16:16:48
-- Design Name: 
-- Module Name: MEM_WB - BehavMEMWB
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MEM_WB is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           -- GPR 
           MEMwd : in STD_LOGIC_VECTOR (4 downto 0);
           MEMwreg : in STD_LOGIC;
           MEMwdata : in STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           MEMwE_hilo: in std_logic;
           MEMhi_data: in std_logic_vector(31 downto 0);
           MEMlo_data: in std_logic_vector(31 downto 0); 
           -- CP0
           MEMcp0_reg_we: in std_logic;
           MEMcp0_reg_waddr: in std_logic_vector(4 downto 0);
           MEMcp0_reg_wdata: in std_logic_vector(31 downto 0);
           -- TLB
           MEMTLBwe: in std_logic;
           MEMTLBrandom: in std_logic;
           -- GPR
           WBwd : out STD_LOGIC_VECTOR (4 downto 0);
           WBwreg : out STD_LOGIC;
           WBwdata : out STD_LOGIC_VECTOR (31 downto 0);
           -- HILO
           WBwE_hilo: out std_logic;
           WBhi_data: out std_logic_vector(31 downto 0);
           WBlo_data: out std_logic_vector(31 downto 0);
           -- CP0
           WBcp0_reg_we: out std_logic;
           WBcp0_reg_waddr: out std_logic_vector(4 downto 0);
           WBcp0_reg_wdata: out std_logic_vector(31 downto 0);
           -- TLB
           WBTLBwe: out std_logic;
           WBTLBrandom: out std_logic;
           --exceptions
           flush: in std_logic;
           -- stalling 
           IDstall: in std_logic;   -- 0 if not stall
           IFstall: in std_logic;
           MEMstall: in std_logic
       );
end MEM_WB;

architecture BehavMEMWB of MEM_WB is

begin
	process(clk, rst)
        variable stall: std_logic;
	begin
		if (rising_edge(clk)) then
            stall := MEMstall;
			if (rst = RstEnable) then
				WBwd <= (others => '0');
				WBwreg <= WriteDisable;
				WBwdata <= Zero32;
                WBwE_hilo <= WriteDisable;
                WBhi_data <= Zero32;
                WBlo_data <= Zero32;
                WBcp0_reg_we <= WriteDisable;
                WBcp0_reg_waddr <= "00000";
                WBcp0_reg_wdata <= Zero32;
                WBTLBwe <= '0';
                WBTLBrandom <= '0';
			elsif (flush = '1') then
                WBwd <= (others => '0');
                WBwreg <= WriteDisable;
                WBwdata <= Zero32;
                WBwE_hilo <= WriteDisable;
                WBhi_data <= Zero32;
                WBlo_data <= Zero32;
                WBcp0_reg_we <= WriteDisable;
                WBcp0_reg_waddr <= "00000";
                WBcp0_reg_wdata <= Zero32;
                WBTLBwe <= '0';
                WBTLBrandom <= '0';
            elsif(stall = '0') then
				WBwd <= MEMwd;
				WBwreg <= MEMwreg;
				WBwdata <= MEMwdata;
                WBwE_hilo <= MEMwE_hilo;
                WBhi_data <= MEMhi_data;
                WBlo_data <= MEMlo_data;
                WBcp0_reg_we <= MEMcp0_reg_we;
                WBcp0_reg_waddr <= MEMcp0_reg_waddr;
                WBcp0_reg_wdata <= MEMcp0_reg_wdata;
                WBTLBwe <= MEMTLBwe;
                WBTLBrandom <= MEMTLBrandom;
            else
                WBwd <= (others => '0');
                WBwreg <= WriteDisable;
                WBwdata <= Zero32;
                WBwE_hilo <= WriteDisable;
                WBhi_data <= Zero32;
                WBlo_data <= Zero32;
                WBcp0_reg_we <= WriteDisable;
                WBcp0_reg_waddr <= "00000";
                WBcp0_reg_wdata <= Zero32;
                WBTLBwe <= '0';
                WBTLBrandom <= '0';
			end if;
		end if;
	end process;

end BehavMEMWB;
