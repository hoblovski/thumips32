----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/10/14 10:51:41
-- Design Name:
-- Module Name: PC - BehavPC
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PC is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           bootFromFlash : in std_logic; 
           BranchFlag : in STD_LOGIC;
           BranchAddr : in STD_LOGIC_VECTOR(31 downto 0);
           IDstall: in STD_LOGIC; -- 0 if cpu does not stall
           IFstall: in STD_LOGIC; -- 0 if cpu does not stall 
           MEMstall: in std_logic;  
           flush : in STD_LOGIC; -- 1 if exceptions are found
           newPC : in STD_LOGIC_VECTOR(31 downto 0);
           pc : out STD_LOGIC_VECTOR (31 downto 0);
           ce : out STD_LOGIC);
end PC;

architecture BehavPC of PC is
	signal ce1: std_logic;
	signal pc1: std_logic_vector (31 downto 0);
	signal stall: std_logic;
begin
	pc <= pc1;
	ce <= ce1;

	process (rst)
	begin
		if (rst = RstEnable) then
			ce1 <= ChipDisable;
		else
			ce1 <= ChipEnable;
		end if;
	end process;

    stall <= IDstall or IFstall or MEMstall;
	
	process (clk)
	begin
		if (rising_edge(clk)) then
			if (ce1 = ChipDisable) then
                if (bootFromFlash) then
                    pc1 <= x"BFC00000";
                else
                    pc1 <= x"80000000";
                end if;
            elsif (flush = '1') then 
            	pc1 <= newPC;
            elsif (stall = '0') then
				if (BranchFlag = Branch) then
				    pc1 <= BranchAddr;
				else
				    pc1 <= std_logic_vector(unsigned(pc1) + 4);
				end if;
			end if;
		end if;
	end process;

end BehavPC;
