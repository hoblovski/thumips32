----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/10/14 11:26:46
-- Design Name:
-- Module Name: Register - BehavReg
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registers is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           waddr : in STD_LOGIC_VECTOR (4 downto 0);
           wdata : in STD_LOGIC_VECTOR (31 downto 0);
           we : in STD_LOGIC;
           raddr1 : in STD_LOGIC_VECTOR (4 downto 0);
           rdata1 : out STD_LOGIC_VECTOR (31 downto 0);
           raddr2 : in STD_LOGIC_VECTOR (4 downto 0);
           rdata2 : out STD_LOGIC_VECTOR (31 downto 0);
           -- debug 
           watchedReg: out DWord 
       );
end Registers;

architecture BehavReg of Registers is
	type Reg is array(31 downto 0) of std_logic_vector(RegWidth-1 downto 0);
	signal regs: Reg;
begin
    watchedReg <=  regs(8)(31 downto 0); -- s3

	process (clk)
	begin
		if (rising_edge(clk)) then
			if (rst = RstDisable) then
				if ((we = WriteEnable) and (waddr /= "00000")) then
					regs(to_integer(unsigned(waddr))) <= wdata;
				end if;
			end if;
		end if;
	end process;

	process (all)
	begin
		if (rst = RstEnable) then
			rdata1 <= Zero32;
		elsif (raddr1 =  "00000") then
			rdata1 <= Zero32;
		elsif ((raddr1 = waddr) and (we = WriteEnable)) then
			rdata1 <= wdata;
        else
			rdata1 <= regs(to_integer(unsigned(raddr1)));
		end if;
	end process;

	process (all)
	begin
		if (rst = RstEnable) then
			rdata2 <= Zero32;
		elsif (raddr2 =  "00000") then
			rdata2 <= Zero32;
		elsif ((raddr2 = waddr) and (we = WriteEnable)) then
			rdata2 <= wdata;
        else
			rdata2 <= regs(to_integer(unsigned(raddr2)));
		end if;
	end process;

end BehavReg;
