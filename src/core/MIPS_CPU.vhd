----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/10/14 16:22:47
-- Design Name:
-- Module Name: MIPS_CPU - BehavMIPSCPU
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MIPS_CPU is
    Port ( clk : in STD_LOGIC;
           romCLK: in STD_LOGIC;
           rst : in STD_LOGIC;
           -- ram
           ramMode: out T_RAMMode;
           ramWData: out DWord;
           ramAddr: out DWord;
           ramRData: in DWord;
           -- com
           COMReceiveData: in std_logic_vector(7 downto 0);
           COMTransmitData: out std_logic_vector(7 downto 0);
           COMTransmitStart: out std_logic;
           COMReceiveReady: in std_logic;-- receive: 1 for ready, 0 for not
           COMTransmitBusy: in std_logic;-- transmit: 1 for busy, 0 for ready
           COMReadyReadTEST: out std_logic;
           COMLastReadTEST: out std_logic;
           COMRDataTEST: out std_logic_vector(7 downto 0);
           -- flash
           FlashRe: out std_logic;
           FlashAddr: out std_logic_vector(31 downto 0);
           FlashPause: in std_logic;
           FlashData: in std_logic_vector(31 downto 0);
           -- timer_int
           int : in STD_LOGIC_VECTOR (5 downto 0);
           timer_int: out STD_LOGIC;
           -- debug
           bootFromFlash: in std_logic;
           watchedReg: out DWord;
           watchTLB: out std_logic_vector(63 downto 0);
           watchTLBwe: out std_logic_vector(5 downto 0);
           watchCP0Index: out std_logic_vector(31 downto 0);
           pc: out DWord;
           inst: out DWord;
           epcTEST: out DWord;
           causeTEST: out DWord;
           statusTEST: out DWord;
           newPCTEST: out DWord;
           flushTEST: out std_logic;
           MEMStallTEST: out std_logic;
           BranchAddrTEST: out DWord;
           pcMoveImm_SignedExtendTEST: out Dword;
           pcPlus4TEST: out Dword;
           IDinstTEST: out Dword;
           ERRORaddrTEST: out Dword
       );
end MIPS_CPU;

architecture BehavMIPSCPU of MIPS_CPU is
    -- IF
    signal IFstall: std_logic;

    -- IF_ID => ID --
    signal IFpc, IFinst, IDpc, IDinst: std_logic_vector(31 downto 0);

    -- ID => ID_EX --
    signal IDALUop: std_logic_vector(7 downto 0);
    signal IDALUselect: std_logic_vector(2 downto 0);
    signal IDnum1, IDnum2: std_logic_vector(31 downto 0);
    signal IDwreg: std_logic;
    signal IDwd: std_logic_vector(4 downto 0);
    signal IDinDelaySlot: std_logic;
    signal IDLinkAddr: std_logic_vector(31 downto 0);
    signal IDNextInDelaySlot: std_logic;
    signal IDramMode: T_RAMMode;
    signal IDramWData: DWord;
    signal IDstall: std_logic;
    signal IDExceptType: std_logic_vector(31 downto 0);
    signal IDCurrentInstAddr: std_logic_vector(31 downto 0);
    signal IDTLBwe, IDTLBrandom: std_logic;

    -- ID_EX => EX --
    signal EXALUop: std_logic_vector(7 downto 0);
    signal EXALUselect: std_logic_vector(2 downto 0);
    signal EXnum1, EXnum2: std_logic_vector(31 downto 0);
    signal EXwreg: std_logic;
    signal EXwd: std_logic_vector(4 downto 0);
    signal EXinDelaySlot: std_logic;
    signal EXLinkAddr: std_logic_vector(31 downto 0);
    signal EXinst: std_logic_vector(31 downto 0);
    signal EXramMode: T_RAMMode;
    signal EXramWData: DWord;
    signal EXExceptType: std_logic_vector(31 downto 0);
    signal EXCurrentInstAddr: std_logic_vector(31 downto 0);
    signal EXTLBwe, EXTLBrandom: std_logic;

    -- EX => EX_MEM --
    signal EXwreg_o: std_logic;
    signal EXwd_o: std_logic_vector(4 downto 0);
    signal EXwdata_o: std_logic_vector(31 downto 0);
    signal EXwE_hilo_o: std_logic;
    signal EXhi_data_o: std_logic_vector(31 downto 0);
    signal EXlo_data_o: std_logic_vector(31 downto 0);
    signal EXcp0_reg_we_o: std_logic;
    signal EXcp0_reg_wdata_o: std_logic_vector(31 downto 0);
    signal EXcp0_reg_waddr_o: std_logic_vector(4 downto 0);
    signal EXramMode_ps: T_RAMMode;
    signal EXramWData_ps: DWord;
    signal EXaluOp_ps: AluOpCode;
    signal EXexceptType_o: std_logic_vector(31 downto 0);
    signal EXcurrentInstAddr_o: std_logic_vector(31 downto 0);
    signal EXisInDelaySlot_o: std_logic;
    signal EXTLBwe_o, EXTLBrandom_o: std_logic;

    -- EX_MEM => MEM --
    signal MEMwreg: std_logic;
    signal MEMwd: std_logic_vector(4 downto 0);
    signal MEMwdata: std_logic_vector(31 downto 0);
    signal MEMwE_hilo: std_logic;
    signal MEMhi_data: std_logic_vector(31 downto 0);
    signal MEMlo_data: std_logic_vector(31 downto 0);
    signal MEMcp0_reg_wdata: std_logic_vector(31 downto 0);
    signal MEMcp0_reg_waddr: std_logic_vector(4 downto 0);
    signal MEMcp0_reg_we: std_logic;
    signal MEMExceptType: std_logic_vector(31 downto 0);
    signal MEMCurrentInstAddr: std_logic_vector(31 downto 0);
    signal MEMIsInDelaySlot: std_logic;
    signal MEMramMode: T_RAMMode;
    signal MEMramWdata, MEMramRdata: std_logic_vector(31 downto 0);
    signal MEMTLBwe, MEMTLBrandom: std_logic;
    signal MEMStall: std_logic;

    -- MEM => MEM_WB --
    signal MEMwreg_o: std_logic;
    signal MEMwd_o: std_logic_vector(4 downto 0);
    signal MEMwdata_o: std_logic_vector(31 downto 0);
    signal MEMwE_hilo_o: std_logic;
    signal MEMhi_data_o: std_logic_vector(31 downto 0);
    signal MEMlo_data_o: std_logic_vector(31 downto 0);
    signal MEMcp0_reg_wdata_o: std_logic_vector(31 downto 0);
    signal MEMcp0_reg_waddr_o: std_logic_vector(4 downto 0);
    signal MEMcp0_reg_we_o: std_logic;
    signal MEMTLBwe_o, MEMTLBrandom_o: std_logic;

    -- MEM => CTRL / CP0 --
    signal cp0_epc_o: std_logic_vector(31 downto 0);
    signal MEMExceptType_o: std_logic_vector(31 downto 0);
    signal MEMCurrentInstAddr_o: std_logic_vector(31 downto 0);
    signal MEMIsInDelaySlot_o: std_logic;

    -- MEM_WB => WB --
    signal WBwreg: std_logic;
    signal WBwd: std_logic_vector(4 downto 0);
    signal WBwdata: std_logic_vector(31 downto 0);
    signal WBwE_hilo: std_logic;
    signal WBhi_data: std_logic_vector(31 downto 0);
    signal WBlo_data: std_logic_vector(31 downto 0);
    signal WBcp0_reg_wdata: std_logic_vector(31 downto 0);
    signal WBcp0_reg_waddr: std_logic_vector(4 downto 0);
    signal WBcp0_reg_we: std_logic;

    -- Register --
    signal reg1_data, reg2_data: std_logic_vector(31 downto 0);
    signal reg1_addr, reg2_addr: std_logic_vector(4 downto 0);

    -- HI/LO --
    signal HILO_rHi: std_logic_vector(31 downto 0);
    signal HILO_rLo: std_logic_vector(31 downto 0);

    -- Branch --
    signal BranchAddr: std_logic_vector(31 downto 0);
    signal BranchFlag: std_logic;
    signal ThisInDelaySlot: std_logic;

    -- CP0 --
    signal cp0_reg_raddr: std_logic_vector(4 downto 0);
    signal cp0_reg_rdata: std_logic_vector(31 downto 0);
    signal cp0_status: std_logic_vector(31 downto 0);
    signal cp0_cause: std_logic_vector(31 downto 0);
    signal cp0_epc: std_logic_vector(31 downto 0);
    signal cp0_index: std_logic_vector(31 downto 0);
    signal cp0_random: std_logic_vector(31 downto 0);
    signal cp0_entrylo0: std_logic_vector(31 downto 0);
    signal cp0_entrylo1: std_logic_vector(31 downto 0);
    signal cp0_entryhi: std_logic_vector(31 downto 0);
    
    -- TLB --
    signal TLB_we: std_logic;
    signal TLB_random: std_logic;
    signal TLB_vaddr: std_logic_vector(31 downto 0);
    signal TLB_paddr: std_logic_vector(31 downto 0);
    signal TLB_miss: std_logic;

    -- CTRL --
    signal flush: std_logic;
    signal newPC: std_logic_vector(31 downto 0);

    -- OTHERS
    signal rom_ce: std_logic;
    signal ExpMiss: std_logic;
    
    -- TEST
    signal pcMoveImm_SignedExtend: std_logic_vector(31 downto 0);
    signal pcPlus4: std_logic_vector(31 downto 0);
    
begin

    pc <= IFpc;
    inst <= IFinst;
    IDinstTEST <= IDinst;
    epcTEST <= cp0_epc;
    newPCTEST <= newPC;
    causeTEST <= cp0_cause;
    statusTEST <= cp0_status;
    flushTEST <= flush;
    MEMStallTEST <= MEMStall;
    BranchAddrTEST <= BranchAddr;
    pcMoveImm_SignedExtendTEST <= pcMoveImm_SignedExtend;
    pcPlus4TEST <= pcPlus4;
    -- PC --
    uPC: entity work.PC port map(
        clk => clk,
        rst => rst,
        bootFromFlash => bootFromFlash,
        BranchFlag => BranchFlag,
        BranchAddr => BranchAddr,
        pc => IFpc,
        ce => rom_ce,
        flush => flush,
        IDstall => IDstall,
        IFstall => IFstall,
        MEMstall => MEMstall,
        newPC => newPC);

    uMMU: entity work.MMU port map(
        clk => clk,
        rCLK=>romCLK,
        rst => rst,
        MEMaddr => MEMwdata,
        MEMMode => MEMramMode,
        IFpc => IFpc,
        IFinst => IFinst,
        ExpMiss => ExpMiss,
        MEMReadData => MEMramRData,
        MEMWriteData => MEMramWData,
        IFstall => IFstall,
        MEMstall => MEMstall,
        ramAddr => ramAddr,
        ramMode => ramMode,
        ramRdata => ramRdata,
        ramWdata => ramWdata,
        TLBvaddr => TLB_vaddr,
        TLBpaddr => TLB_paddr,
        TLBmiss => TLB_miss,
        COMReceiveData => COMReceiveData,
        COMTransmitData => COMTransmitData,
        COMTransmitStart => COMTransmitStart,
        COMReceiveReady => COMReceiveReady,
        COMTransmitBusy => COMTransmitBusy,
        FlashRe => FlashRe,
        FlashAddr => FlashAddr,
        FlashPause => FlashPause,
        FlashData => FlashData,
        
                   COMReadyReadTEST => COMReadyReadTEST,
                   COMLastReadTEST => COMLastReadTEST,
                   COMRDataTEST => COMRDataTEST,
                   EXaddr => EXCurrentInstAddr_o,
                   ERRORaddrTEST => ERRORaddrTEST
    );
    
    -- IF_ID  --
    uIF_ID: entity work.IF_ID port map(
        rst => rst,
        clk => clk,
        IFpc => IFpc,
        IFinst => IFinst,
        IDpc => IDpc,
        IDinst => IDinst,
        IDstall => IDstall,
        IFstall => IFstall,
        MEMstall => MEMstall,
        flush => flush
    );

    -- ID --
    uID: entity work.ID port map(
        rst => rst,
        pc => IDpc,
        inst => IDinst,
        reg1_data => reg1_data,
        reg2_data => reg2_data,
        reg1_addr_o => reg1_addr,
        reg2_addr_o => reg2_addr,
        ALUop => IDALUop,
        ALUselect => IDALUselect,
        num1_o => IDnum1,
        num2_o => IDnum2,
        wd => IDwd,
        wreg => IDwreg,
        EXwreg => EXwreg_o,
        EXwdata => EXwdata_o,
        EXwd => EXwd_o,
        MEMwreg => MEMwreg_o,
        MEMwdata => MEMwdata_o,
        MEMwd => MEMwd_o,
        isInDelaySlot => ThisInDelaySlot,
        isInDelaySlot_o => IDinDelaySlot,
        NextInDelaySlot => IDNextInDelaySlot,
        BranchFlag => BranchFlag,
        BranchAddr => BranchAddr,
        LinkAddr => IDLinkAddr,
        ramMode => IDramMode,
        ramWData => IDramWData,
        TLBwe => IDTLBwe,
        TLBrandom => IDTLBrandom,
        EXaluOp => EXaluOp_ps,
        IDstall => IDstall,
        exceptType => IDexceptType,
        currentInstAddr => IDCurrentInstAddr,
        
        pcMoveImm_SignedExtendTEST => pcMoveImm_SignedExtend,
        pcPlus4TEST => pcPlus4
    );

    -- ID_EX --
    uID_EX: entity work.ID_EX port map(
        rst => rst,
        clk => clk,
        IDALUselect => IDALUselect,
        IDALUop => IDALUop,
        IDnum1 => IDnum1,
        IDnum2 => IDnum2,
        IDwd => IDwd,
        IDwreg => IDwreg,
        IDLinkAddr => IDLinkAddr,
        IDInDelaySlot => IDInDelaySlot,
        IDNextInDelaySlot => IDNextInDelaySlot,
        IDinst => IDinst,
        IDTLBwe => IDTLBwe,
        IDTLBrandom => IDTLBrandom,
        EXLinkAddr => EXLinkAddr,
        EXInDelaySlot => EXInDelaySlot,
        isInDelaySlot => ThisInDelaySlot,
        EXALUselect => EXALUselect,
        EXALUop => EXALUop,
        EXnum1 => EXnum1,
        EXnum2 => EXnum2,
        EXwd => EXwd,
        EXwreg => EXwreg,
        EXinst => EXinst,
        EXTLBwe => EXTLBwe,
        EXTLBrandom => EXTLBrandom,
        IDramMode => IDramMode,
        IDramWData => IDramWData,
        EXramMode => EXramMode,
        EXramWData => EXramWData,
        -- Exceptions
        flush => flush,
        IDExceptType => IDExceptType,
        IDCurrentInstAddr => IDCurrentInstAddr,
        EXExceptType => EXExceptType,
        EXCurrentInstAddr => EXCurrentInstAddr,
        -- stalling
        IDstall => IDstall,
        IFstall => IFstall,
        MEMstall => MEMstall
    );

    -- EX --
    uEX: entity work.EX port map(
        rst => rst,
        ALUselect => EXALUselect,
        ALUop => EXALUop,
        aluOp_ps => EXaluOp_ps,
        num1 => EXnum1,
        num2 => EXnum2,
        wd => EXwd,
        wreg => EXwreg,
        wd_o => EXwd_o,
        wreg_o => EXwreg_o,
        wdata_o => EXwdata_o,
        inst => EXinst,
        -- value from Hi/LO RF
        hi_rf => HILO_rHi,
        lo_rf => HILO_rLo,
        -- forwarding HI / LO (WB forwarding done in HILO_RF)
        MEM_fw_hi => MEMhi_data_o,
        MEM_fw_lo => MEMlo_data_o,
        MEM_fw_wE_hilo => MEMwE_hilo_o,
        -- forwarding CP0 registers
        MEM_cp0_reg_we => MEMcp0_reg_we_o,
        MEM_cp0_reg_waddr => MEMcp0_reg_waddr_o,
        MEM_cp0_reg_wdata => MEMcp0_reg_wdata_o,
        -- write back to CP0 registers
        wb_cp0_reg_we => WBcp0_reg_we,
        wb_cp0_reg_waddr => WBcp0_reg_waddr,
        wb_cp0_reg_wdata => WBcp0_reg_wdata,
        -- write enable of HILORF for this inst
        wE_hilo => EXwE_hilo_o,
        hi_data => EXhi_data_o,
        lo_data => EXlo_data_o,
        isInDelaySlot => EXinDelaySlot,
        LinkAddr => EXLinkAddr,
        -- transport in pipeline to write to CP0 registers
        cp0_reg_we => EXcp0_reg_we_o,
        cp0_reg_waddr => EXcp0_reg_waddr_o,
        cp0_reg_wdata => EXcp0_reg_wdata_o,
        -- directly read from CP0 registers
        cp0_reg_raddr => cp0_reg_raddr,
        cp0_reg_rdata => cp0_reg_rdata,
        -- RAM
        ramMode => EXramMode,
        ramWData => EXramWData,
        ramMode_ps => EXramMode_ps,
        ramWData_ps => EXramWData_ps,
        -- TLB
        TLBwe => EXTLBwe,
        TLBrandom => EXTLBrandom,
        TLBwe_o => EXTLBwe_o,
        TLBrandom_o => EXTLBrandom_o,
        -- Exceptions
        ExceptType => EXExceptType,
        CurrentInstAddr => EXCurrentInstAddr,
        ExceptType_o => EXExceptType_o,
        CurrentInstAddr_o => EXCurrentInstAddr_o,
        isInDelaySlot_o => EXisInDelaySlot_o
    );

    -- EX_MEM --
    uEX_MEM: entity work.EX_MEM port map(
        rst => rst,
        clk => clk,
        -- GPR
        EXwd => EXwd_o,
        EXwreg => EXwreg_o,
        EXwdata => EXwdata_o,
        -- HILO
        EXwE_hilo => EXwE_hilo_o,
        EXhi_data => EXhi_data_o,
        EXlo_data => EXlo_data_o,
        -- CP0
        EXcp0_reg_wdata => EXcp0_reg_wdata_o,
        EXcp0_reg_waddr => EXcp0_reg_waddr_o,
        EXcp0_reg_we => EXcp0_reg_we_o,
        -- TLB
        EXTLBwe => EXTLBwe_o,
        EXTLBrandom => EXTLBrandom_o,
        -- GPR
        MEMwd => MEMwd,
        MEMwreg => MEMwreg,
        MEMwdata => MEMwdata,
        -- HILO
        MEMwE_hilo => MEMwE_hilo,
        MEMhi_data => MEMhi_data,
        MEMlo_data => MEMlo_data,
        -- CP0
        MEMcp0_reg_wdata => MEMcp0_reg_wdata,
        MEMcp0_reg_waddr => MEMcp0_reg_waddr,
        MEMcp0_reg_we => MEMcp0_reg_we,
        -- TLB
        MEMTLBwe => MEMTLBwe,
        MEMTLBrandom => MEMTLBrandom,
        -- RAM
        EXramMode => EXramMode_ps,
        EXramWData => EXramWData_ps,
        MEMramMode => MEMramMode,     -- directly connect to RAM entity
        MEMramWData => MEMramWdata,     -- clocked ram ctrl
        -- Exceptions
        flush => flush,
        EXExceptType => EXExceptType_o,
        EXCurrentInstAddr => EXCurrentInstAddr_o,
        EXIsInDelaySlot => EXIsInDelaySlot_o,
        MEMExceptType => MEMExceptType,
        MEMCurrentInstAddr => MEMCurrentInstAddr,
        MEMIsInDelaySlot => MEMIsInDelaySlot,
        -- stalling
        IDstall => IDstall,
        IFstall => IFstall,
        MEMstall => MEMstall
    );

    -- MEM --
    uMEM: entity work.MEM port map(
        rst => rst,
        -- write GPR
        wd => MEMwd,
        wreg => MEMwreg,
        wdata => MEMwdata,
        -- write HILO
        wE_hilo => MEMwE_hilo,
        hi_data => MEMhi_data,
        lo_data => MEMlo_data,
        -- CP0
        cp0reg_we => MEMcp0_reg_we,
        cp0reg_waddr => MEMcp0_reg_waddr,
        cp0reg_wdata => MEMcp0_reg_wdata,
        -- GPR
        wd_o => MEMwd_o,
        wreg_o => MEMwreg_o,
        wdata_o => MEMwdata_o,
        -- HILO
        wE_hilo_o => MEMwE_hilo_o,
        hi_data_o => MEMhi_data_o,
        lo_data_o => MEMlo_data_o,
        -- CP0
        cp0reg_we_o => MEMcp0_reg_we_o,
        cp0reg_waddr_o => MEMcp0_reg_waddr_o,
        cp0reg_wdata_o => MEMcp0_reg_wdata_o,
        -- RAM
        ramMode => MEMramMode,
        ramRData => MEMramRdata,
        -- TLB
        TLBmiss => ExpMiss,
        TLBwe => MEMTLBwe,
        TLBrandom => MEMTLBrandom,
        TLBwe_o => MEMTLBwe_o,
        TLBrandom_o => MEMTLBrandom_o,
        -- Exceptions
        ExceptType => MEMExceptType,
        CurrentInstAddr => MEMCurrentInstAddr,
        IsInDelaySlot => MEMIsInDelaySlot,
        cp0Status_i => cp0_status,
        cp0Cause_i => cp0_cause,
        cp0Epc_i => cp0_Epc,
        wbCp0RegWe => WBCp0_reg_we,
        wbCp0RegWaddr => WBcp0_reg_waddr,
        wbCp0RegWdata => WBcp0_reg_wdata,
        cp0Epc_o => cp0_Epc_o,
        exceptType_o => MEMExceptType_o,
        currentInstAddr_o => MEMCurrentInstAddr_o,
        isInDelaySlot_o => MEMIsInDelaySlot_o
    );

    -- MEM_WB --
    uMEM_WB: entity work.MEM_WB port map(
        rst => rst,
        clk => clk,
        -- GPR
        MEMwd => MEMwd_o,
        MEMwreg => MEMwreg_o,
        MEMwdata => MEMwdata_o,
        -- HILO
        MEMwE_hilo => MEMwE_hilo_o,
        MEMhi_data => MEMhi_data_o,
        MEMlo_data => MEMlo_data_o,
        -- CP0
        MEMcp0_reg_we => MEMcp0_reg_we_o,
        MEMcp0_reg_waddr => MEMcp0_reg_waddr_o,
        MEMcp0_reg_wdata => MEMcp0_reg_wdata_o,
        -- TLB
        MEMTLBwe => MEMTLBwe_o,
        MEMTLBrandom => MEMTLBrandom_o,
        -- GPR
        WBwd => WBwd,
        WBwreg => WBwreg,
        WBwdata => WBwdata,
        -- HILO
        WBwE_hilo => WBwE_hilo,
        WBhi_data => WBhi_data,
        WBlo_data => WBlo_data,
        -- CP0
        WBcp0_reg_we => WBcp0_reg_we,
        WBcp0_reg_waddr => WBcp0_reg_waddr,
        WBcp0_reg_wdata => WBcp0_reg_wdata,
        -- TLB
        WBTLBwe => TLB_we,
        WBTLBrandom => TLB_random,
        -- Exceptions
        flush => flush,
        -- stalling
        IDstall => IDstall,
        IFstall => IFstall,
        MEMstall => MEMstall
        );

    -- Registers --
    uRegisters: entity work.Registers port map(
        rst => rst,
        clk => clk,
        waddr => WBwd,
        wdata => Wbwdata,
        we => WBwreg,
        raddr1 => reg1_addr,
        rdata1 => reg1_data,
        raddr2 => reg2_addr,
        rdata2 =>reg2_data,
        watchedReg => watchedReg
    );

    -- HI/LO Register --
    uHILORF: entity work.HiLoRegFile port map (
        rst => rst,
        clk => clk,
        wE => WBwE_hilo,
        wHi => WBhi_data,
        wLo => WBlo_data,
        rHi => HILO_rHi,
        rLo => HILO_rLo);

    -- CP0 --
    uCP0: entity work.CP0_Reg port map (
        rst => rst,
        clk => clk,
        int => int,
        raddr => cp0_reg_raddr,
        rdata => cp0_reg_rdata,
        wdata => WBcp0_reg_wdata,
        waddr => WBcp0_reg_waddr,
        we => WBcp0_reg_we,
        index => cp0_index,
        random => cp0_random,
        entrylo0 => cp0_entrylo0,
        entrylo1 => cp0_entrylo1,
        entryhi => cp0_entryhi,
        status => cp0_status,
        cause => cp0_cause,
        epc => cp0_epc,
        timer_int => timer_int,
        -- Exceptions
        exceptType => MEMExceptType_o,
        currentInstAddr => MEMCurrentInstAddr_o,
        isInDelaySlot => MEMIsInDelaySlot_o,
        -- TLBmiss
        TLBmiss => ExpMiss,
        vaddr => TLB_vaddr
    );
    
    -- TLB --
    uTLB: entity work.TLB port map(
        clk => clk,
        rst => rst,
        CP0Index => cp0_index,
        CP0Random => cp0_random,
        CP0EntryLo0 => cp0_entrylo0,
        CP0EntryLo1 => cp0_entrylo1,
        CP0EntryHi => cp0_entryhi,
        we => TLB_we,
        TLBrandom => TLB_random,
        vaddr => TLB_vaddr,
        paddr => TLB_paddr,
        watchTLB => watchTLB,
        TLBmiss => TLB_miss
    );
    watchTLBwe <= IDTLBwe & EXTLBwe & EXTLBwe_o & MEMTLBwe & MEMTLBwe_o & TLB_we;
    watchCP0Index <= cp0_entrylo1;

    -- CTRL --
    uCTRL: entity work.CTRL port map (
        rst => rst,
        cp0Epc => cp0_epc_o,
        ExceptType => MEMExceptType_o,
        newPC => newPC,
        flush => flush
    );
end BehavMIPSCPU;
