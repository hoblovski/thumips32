----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/11/06 21:29:13
-- Design Name:
-- Module Name: RamController - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.Consts.all;

entity RamController is
    Port (
           clk: in std_logic; -- dummy clock

           -- used in CPU internal interaction
           vaddr: in DWord;     -- virtual address, now mapping done here
           rdata: out DWord;    -- 'Z..Z' for invalid read
           wdata: in DWord;     -- byte write only use wdata(7 downto 0)
           mode: in T_RAMMode;  -- compatibility

           -- port interaction
    	   base_ram_addr: out std_logic_vector(19 downto 0);
           base_ram_data: inout std_logic_vector(31 downto 0);
           base_ram_be_n: out std_logic_vector(3 downto 0);
           base_ram_ce_n: out std_logic;
           base_ram_oe_n: out std_logic;
           base_ram_we_n: out std_logic;
           ext_ram_addr: out std_logic_vector(19 downto 0);
           ext_ram_data: inout std_logic_vector(31 downto 0);
           ext_ram_be_n: out std_logic_vector(3 downto 0);
           ext_ram_ce_n: out std_logic;
           ext_ram_oe_n: out std_logic;
           ext_ram_we_n: out std_logic;
           -- ram hard wired to LEDs 
               -- *** only sw will write to hard-wired. ***
               -- *** sb write to memory. ***
           leds: out std_logic_vector(31 downto 0)
       );
end RamController;

architecture Behavioral of RamController is
    signal paddr: DWord;
    signal ledWE: std_logic;
    signal ram_data: DWord;

    signal l_vaddr: DWord;
    signal l_wdata: DWord;
    signal l_mode: T_RAMMode;

begin
    -- buffer the inputs
    process (clk)
    begin
        if (rising_edge(clk)) then
            l_vaddr <= vaddr;
            l_wdata <= wdata;
            l_mode <= mode;
        end if;
    end process;

    -- v/p address mapping
    paddr <= '0' & l_vaddr(30 downto 0);

	base_ram_addr <= paddr(21 downto 2);
	ext_ram_addr <= paddr(21 downto 2);
	base_ram_ce_n <= paddr(22);
	ext_ram_ce_n <= not paddr(22);
	base_ram_oe_n <= '0';
	ext_ram_oe_n <= '0';

    process (all)
    begin
        case paddr(22) is
            when '0' => ram_data <= base_ram_data;
            when '1' => ram_data <= ext_ram_data;
        end case;
    end process;

    --process (paddr, l_wdata, l_mode, ram_data)
    process (all)
    begin
        base_ram_be_n <= "0000";
        ext_ram_be_n <= "0000";
        ledWE <= '0';
        rdata <= (others=> '0');

        case l_mode is

            when RAMMODE_WRITE_WORD =>
                -- alignation test
                if (paddr(1 downto 0) /= "00") then
                    -- TODO: issue error: unaligned write
                    null;
                end if;

                base_ram_we_n <= '0';
                ext_ram_we_n <= '0';
                base_ram_data <= l_wdata;
                ext_ram_data <= l_wdata;

            when RAMMODE_WRITE_BYTE =>
                base_ram_we_n <= '0';
                ext_ram_we_n <= '0';
                -- ram_data is write to aligned position

                case paddr(1 downto 0) is
                    when "00" =>
                        base_ram_be_n <= "1110";
                        ext_ram_be_n <= "1110";
--                        base_ram_data <= std_logic_vector'(x"000000") & l_wdata(7 downto 0);
--                        ext_ram_data <= std_logic_vector'(x"000000") & l_wdata(7 downto 0);
                        base_ram_data(7 downto 0) <= l_wdata(7 downto 0);
                        ext_ram_data(7 downto 0) <= l_wdata(7 downto 0);
                    when "01" =>
                        base_ram_be_n <= "1101";
                        ext_ram_be_n <= "1101";
--                        base_ram_data <= std_logic_vector'(x"0000") & l_wdata(7 downto 0) & std_logic_vector'(x"00");
--                        ext_ram_data <= std_logic_vector'(x"0000") & l_wdata(7 downto 0) & std_logic_vector'(x"00");
                        base_ram_data(15 downto 8) <= l_wdata(7 downto 0);
                        ext_ram_data(15 downto 8) <= l_wdata(7 downto 0);
                    when "10" =>
                        base_ram_be_n <= "1011";
                        ext_ram_be_n <= "1011";
--                        base_ram_data <= std_logic_vector'(x"00") & l_wdata(7 downto 0) & std_logic_vector'(x"0000");
--                        ext_ram_data <= std_logic_vector'(x"00") & l_wdata(7 downto 0) & std_logic_vector'(x"0000");
                        base_ram_data(23 downto 16) <= l_wdata(7 downto 0);
                        ext_ram_data(23 downto 16) <= l_wdata(7 downto 0);
                    when "11" =>
                        base_ram_be_n <= "0111";
                        ext_ram_be_n <= "0111";
--                        base_ram_data <= std_logic_vector'(x"00") & l_wdata(7 downto 0) & std_logic_vector'(x"0000");
--                        ext_ram_data <= std_logic_vector'(x"00") & l_wdata(7 downto 0) & std_logic_vector'(x"0000");
                        base_ram_data(31 downto 24) <= l_wdata(7 downto 0);
                        ext_ram_data(31 downto 24) <= l_wdata(7 downto 0);
                end case;

            when RAMMODE_READ_WORD =>
                if (paddr(1 downto 0) /= "00") then
                    -- TODO: issue error: unaligned read
                    null;
                end if;

                base_ram_we_n <= '1';
                ext_ram_we_n <= '1';
                base_ram_data <= (others=> 'Z');
                ext_ram_data <= (others=> 'Z');

                rdata <= ram_data;

            when RAMMODE_READ_BYTE =>
                base_ram_we_n <= '1';
                ext_ram_we_n <= '1';
                base_ram_data <= (others=> 'Z');
                ext_ram_data <= (others=> 'Z');

                -- ram_data is read at aligned position, not really at paddr
                case paddr(1 downto 0) is
                    when "00" => rdata <= sign_extend(ram_data(7 downto 0));
                    when "01" => rdata <= sign_extend(ram_data(15 downto 8));
                    when "10" => rdata <= sign_extend(ram_data(23 downto 16));
                    when "11" => rdata <= sign_extend(ram_data(31 downto 24));
                end case;

            when RAMMODE_READ_BYTEU =>
                base_ram_we_n <= '1';
                ext_ram_we_n <= '1';
                base_ram_data <= (others=> 'Z');
                ext_ram_data <= (others=> 'Z');

                -- ram_data is read at aligned position, not really at paddr
                case paddr(1 downto 0) is
                    when "00" => rdata <= zero_extend(ram_data(7 downto 0));
                    when "01" => rdata <= zero_extend(ram_data(15 downto 8));
                    when "10" => rdata <= zero_extend(ram_data(23 downto 16));
                    when "11" => rdata <= zero_extend(ram_data(31 downto 24));
                end case;

            when others =>
                base_ram_we_n <= '1';
                ext_ram_we_n <= '1';
                base_ram_data <= (others=> 'Z');
                ext_ram_data <= (others=> 'Z');

        end case;
    end process;

end Behavioral;
