----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/11/05 13:46:29
-- Design Name: 
-- Module Name: FlashController - BehavFlash
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.consts.ALL;

entity FlashController is
    Port ( 
           rst: in std_logic;
           clk: in std_logic;
           addr: in std_logic_vector(31 downto 0);
           re: in std_logic;
           pause: out std_logic;
           data: out std_logic_vector(15 downto 0);
           
           
   	       flash_a : out STD_LOGIC_VECTOR (22 downto 0);
           flash_rp_n : out STD_LOGIC;
           flash_vpen : out STD_LOGIC;
           flash_oe_n : out STD_LOGIC;
           flash_we_n : out STD_LOGIC;
           flash_ce_n : out STD_LOGIC;
           flash_byte_n : out STD_LOGIC;
           flash_data : inout STD_LOGIC_VECTOR (15 downto 0));
end FlashController;

architecture BehavFlash of FlashController is
    signal state: integer;
begin
    flash_ce_n <= '0';
    flash_byte_n <= '1';
    flash_we_n <= '1';
    flash_vpen <= '1';
    flash_rp_n <= not rst;
    flash_a(0) <= '0';
    flash_a(22 downto 1) <= addr(23 downto 2);
    flash_data <= "ZZZZZZZZZZZZZZZZ";
    
    process(all)
    begin
       if(rst = RstEnable) then
            pause <= '0';
            flash_oe_n <= '1';
            state <= 0;
            data<= x"0000";
       elsif(rising_edge(clk)) then
            if(state = 0) then 
                if( re = '1') then
                    pause <= '1';
                    flash_oe_n <= '0';
                    state <= 1;
                else
                    state <= 0; 
                    pause <= '0';
                    data <= x"0000";
                    flash_oe_n <= '1';
                end if;
            else
                data <= flash_data;
                state <= 0;
                pause <= '0';
                flash_oe_n <= '1';
            end if;
        end if;
    end process;
    
end BehavFlash;
