----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/11/05 14:43:05
-- Design Name: 
-- Module Name: TestPeripheral - BehavPeripheral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TestPeripheral is
  Port ( 
           touch_btn: in std_logic_vector(5 downto 0);
           clk: in std_logic;
           clk_uart: in std_logic;
           leds: out std_logic_vector(31 downto 0);
           dip_sw: in std_logic_vector(31 downto 0);
           
           txd: out std_logic;
           rxd: in std_logic;
           
           base_ram_addr: out std_logic_vector(19 downto 0);
           base_ram_data: inout std_logic_vector(31 downto 0);
           base_ram_be_n: out std_logic_vector(3 downto 0);
           base_ram_ce_n: out std_logic;
           base_ram_oe_n: out std_logic;
           base_ram_we_n: out std_logic;
           ext_ram_addr: out std_logic_vector(19 downto 0);
           ext_ram_data: inout std_logic_vector(31 downto 0);
           ext_ram_be_n: out std_logic_vector(3 downto 0);
           ext_ram_ce_n: out std_logic;
           ext_ram_oe_n: out std_logic;
           ext_ram_we_n: out std_logic;
           
           flash_a : out STD_LOGIC_VECTOR (22 downto 0);
           flash_rp_n : out STD_LOGIC;
           flash_vpen : out STD_LOGIC;
           flash_oe_n : out STD_LOGIC;
           flash_we_n : out STD_LOGIC;
           flash_ce_n : out STD_LOGIC;
           flash_byte_n : out STD_LOGIC;
           flash_data : inout STD_LOGIC_VECTOR (15 downto 0)
  );
end TestPeripheral;

architecture BehavPeripheral of TestPeripheral is
    component async_transmitter generic(
        ClkFrequency: integer;
        Baud: integer);
    port(
        clk: in std_logic;
        TxD_start: in std_logic;
        TxD_data: in std_logic_vector(7 downto 0);
        TxD: out std_logic;
        TxD_busy: out std_logic
    );
    end component;
    component async_receiver generic(
        ClkFrequency: integer;
        Baud: integer);
    port(
        clk: in std_logic;
        RxD_data: out std_logic_vector(7 downto 0);
        RxD: in std_logic;
        RxD_idle: out std_logic;
        RxD_endofpacket: out std_logic;
        RxD_data_ready: out std_logic
    );
    end component;

    signal addr : std_logic_vector(31 downto 0);
    signal re, pause: std_logic;
    signal data: std_logic_vector(15 downto 0);
    
    signal ram_data_read: std_logic_vector(31 downto 0);
    signal ram_data_write: std_logic_vector(31 downto 0);
    signal ram_wr: std_logic;
    
    signal RxD_data_ready: std_logic;
    signal RxD_data: std_logic_vector(7 downto 0);
    
begin
    leds(15 downto 0) <= data;
    leds(16) <= re;
    --leds(24) <= ram_wr;
    --leds <= ram_data_read;
    ram_data_write <= x"0000" & data;
    
    uFlash: entity work.FlashController port map (
        rst => touch_btn(4),
        clk => not touch_btn(5),
        addr => addr,
        re => re,
        pause => pause,
        data => data,
        flash_a => flash_a,
        flash_rp_n => flash_rp_n,
        flash_vpen => flash_vpen,
        flash_oe_n => flash_oe_n,
        flash_we_n => flash_we_n,
        flash_ce_n => flash_ce_n,
        flash_byte_n => flash_byte_n,
        flash_data => flash_data
    );
    uRam: entity work.RamController port map (
    	base_ram_addr => base_ram_addr,
    	base_ram_data => base_ram_data,
    	base_ram_be_n => base_ram_be_n,
    	base_ram_ce_n => base_ram_ce_n,
    	base_ram_oe_n => base_ram_oe_n,
    	base_ram_we_n => base_ram_we_n,
    	ext_ram_addr => ext_ram_addr,
    	ext_ram_data => ext_ram_data,
    	ext_ram_be_n => ext_ram_be_n,
    	ext_ram_ce_n => ext_ram_ce_n,
    	ext_ram_oe_n => ext_ram_oe_n,
    	ext_ram_we_n => ext_ram_we_n,
    	data_read => ram_data_read,
    	data_write => ram_data_write,
    	wr => ram_wr,
    	addr => addr
    );
    
    uAsyncTransmitter: async_transmitter generic map(
        ClkFrequency => 11059200,
        Baud => 115200
    )
    port map(
        clk => clk_uart,
        TxD => txd,
        TxD_start => RxD_data_ready,--RxD_data_ready,
        TxD_data => data(7 downto 0)--RxD_data
    );
    
    uAsyncReceiver: async_receiver generic map(
        ClkFrequency => 11059200,
        Baud => 115200
    )
    port map(
        clk => clk_uart,
        RxD => rxd,
        RxD_data_ready => RxD_data_ready,
        RxD_data => RxD_data
    );
    
    
    process(all) -- flash read 
    begin 
    	if (touch_btn(4) = '1') then
    		addr <= x"00000000";
    		re <= '0';
        elsif (rising_edge(touch_btn(5))) then
            if( pause = '0') then
                ram_wr <= '1';
                addr <= std_logic_vector(unsigned(addr) + 2);
                re <= '1';
            else
                ram_wr <= '0';
                re <= '0';
            end if;
        end if;
    end process;
    
    --ram_wr <= '0';

end BehavPeripheral;
