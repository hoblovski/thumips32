----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 2017/11/22 19:51:22
-- Design Name:
-- Module Name: MMU - BehavMMU
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.consts.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MMU is
    Port ( clk : in std_logic;
           rclk: in std_logic;
           rst : in std_logic;
           MEMaddr : in STD_LOGIC_VECTOR (31 downto 0);
           MEMMode : in T_RAMMode;
           IFpc : in STD_LOGIC_VECTOR (31 downto 0);
           IFinst: out std_logic_vector(31 downto 0);
           MEMReadData : out std_logic_vector(31 downto 0);
           MEMWritedata : in std_logic_vector(31 downto 0);
           IFstall : out STD_LOGIC;
           MEMstall : out std_logic;
           ExpMiss: out std_logic;

           ramAddr : out std_logic_vector(31 downto 0);
           ramMode : out T_RAMMode;
           ramRdata : in std_logic_vector(31 downto 0);
           ramWdata : out std_logic_vector(31 downto 0);

           TLBvaddr : out std_logic_vector(31 downto 0);
           TLBpaddr : in std_logic_vector(31 downto 0);
           TLBmiss : in std_logic;

           COMReceiveData: in std_logic_vector(7 downto 0);
           COMTransmitData: out std_logic_vector(7 downto 0);
           COMTransmitStart: out std_logic;
           COMReceiveReady: in std_logic;-- receive: 1 for ready, 0 for not
           COMTransmitBusy: in std_logic;-- transmit: 1 for busy, 0 for ready

           FlashRe: out std_logic;
           FlashAddr: out std_logic_vector(31 downto 0);
           FlashPause: in std_logic;
           FlashData: in std_logic_vector(31 downto 0);

           COMReadyReadTEST: out std_logic;
           COMLastReadTEST: out std_logic;

           COMRDataTEST: out std_logic_vector(7 downto 0);

           EXaddr: in std_logic_vector(31 downto 0);
           ERRORaddrTEST: out std_logic_vector(31 downto 0)
           );
end MMU;

architecture BehavMMU of MMU is

    COMPONENT ROMboot
      PORT (
        clka : IN STD_LOGIC;
        addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END component ;

    signal comRdata, comWdata: std_logic_vector(7 downto 0);
    signal comReadyRead, comLastRead: std_logic;
    signal comReadySend, comLastSend: std_logic;
    signal COMdata: std_logic_vector(31 downto 0);
    signal ROMclk: std_logic;
    signal ROMaddr: std_logic_vector(7 downto 0);
    signal ROMdata: std_logic_vector(31 downto 0);
    signal COMTransmitStart1: std_logic;

    type Device is (dROM, dRAM, dFlash, dCOM, dTLB, dNOP);
    signal IFDevice, MEMDevice: Device;

    signal ERRORaddrTEST1: std_logic_vector(31 downto 0);

begin

    uROM: ROMboot port map(
        clka => ROMclk,
        addra => ROMaddr,
        douta => ROMdata
    );

    --IFinst <= ramRdata;
    ROMclk <= rclk;
    ramWdata <= MEMWritedata;
    --TLBvaddr <= MEMaddr;
    COMTransmitStart <= COMTransmitStart1;
    comReadyReadTEST <= comReadyRead;
    comLastReadTEST <= comLastRead;
    COMRDataTEST <=comRdata;
    ERRORaddrTEST <= ERRORaddrTEST1;
    MEMstall <= FlashPause;

    process(all) -- IF
    begin
        if(rst = RstEnable) then
            IFDevice <= dNOP;
            IFinst <= Zero32;
            ERRORaddrTEST1 <= Zero32;
        else
            if(IFpc(31 downto 24) = x"80") then
                IFDevice <= dRAM;
                IFinst <= ramRdata;
            elsif(IFpc(31 downto 24) = x"BE") then
                IFDevice <= dFlash;
                IFinst <= Flashdata;
            elsif(IFpc(31 downto 12) = x"BFC00") then
                IFDevice <= dROM;
                IFinst <= ROMdata;
            elsif(IFpc = x"BFD003F8" or IFpc = x"BFD003FC") then
                IFDevice <= dCOM;
                IFinst <= COMdata;
                -- pc should not from COM...
            elsif(IFpc(31)='0' or IFpc(31 downto 30) ="11") then
                IFDevice <= dTLB;
                IFinst <= RamRData;
                if (ERRORaddrTEST1 = Zero32) then
                    ERRORaddrTEST1 <= EXaddr;
                end if;
            else
                IFDevice <= dNOP;
                IFinst <= ramRdata;
            end if;
        end if;
    end process;

    process(all) -- MEM
    begin
        if(rst = RstEnable) then
            MEMDevice <= dNOP;
            MEMReadData <= Zero32;
        elsif(MEMMode = RAMMODE_NOP) then
            MEMDevice <= dNOP;
            MEMReadData <= Zero32;
        else
            if(MEMaddr(31 downto 24) = x"80") then
                MEMDevice <= dRAM;
                MEMReadData <= ramRdata;
            elsif(MEMaddr(31 downto 24) = x"BE") then
                MEMDevice <= dFlash;
                MEMReadData <= Flashdata;
            elsif(MEMaddr(31 downto 12) = x"BFC00") then
                MEMDevice <= dROM;
                MEMReadData <= ROMdata;
            elsif(MEMaddr = x"BFD003F8" or MEMaddr = x"BFD003FC") then
                MEMDevice <= dCOM;
                MEMReadData <= COMdata;
            elsif(MEMaddr(31)='0' or MEMaddr(31 downto 30) ="11") then
                MEMDevice <= dTLB;
                MEMReadData <= RamRData;
            else
                MEMDevice <= dNOP;
                MEMReadData <= ramRdata;
            end if;
        end if;
    end process;

    process(all) -- Stall
    begin
        if(MEMDevice = IFDevice) then
            IFStall <= '1';
        elsif(MEMDevice = dTLB and IFDevice = dRAM) then
            IFStall <= '1';
        elsif(MEMDevice = dRAM and IFDevice = dTLB) then
            IFStall <= '1';
        else
            IFStall <= '0';
        end if;
    end process;

    process(all) -- ROM
    begin
        if(rst = RstEnable) then
            ROMaddr <= x"00";
        elsif(MEMDevice = dROM) then
            ROMaddr <= MEMaddr(9 downto 2);
        elsif(IFDevice = dROM) then
            ROMaddr <= IFpc(9 downto 2);
        else
            ROMaddr <= x"00";
        end if;
    end process;

    process(all) -- COM
    begin
        if(rst = RstEnable) then
            comLastRead <= '1';
            comLastSend <= '1';
        elsif(MEMDevice = dCOM) then
            if (MEMaddr = x"BFD003F8") then
                case MEMMode is
                    when RAMMODE_READ_BYTE =>
                        COMData <= sign_extend(comRdata);
                        if(comReadyRead /= comLastRead) then
                            comLastRead <= comReadyRead;
                        else
                            -- error
                        end if;
                    when RAMMODE_READ_BYTEU =>
                        COMData <= zero_extend(comRdata);
                        if(comReadyRead /= comLastRead) then
                            comLastRead <= comReadyRead;
                        else
                            -- error
                        end if;
                    when RAMMODE_WRITE_BYTE =>
                        if(COMTransmitBusy = '0' and (comLastSend /= comReadySend)) then
                            COMTransmitData <= MEMWritedata(7 downto 0);
                            comLastSend <= comReadySend;
                        else
                            --error
                        end if;
                    when others =>
                            --error
                end case;
            elsif (MEMaddr = x"BFD003FC") then
                case MEMMode is
                    when RAMMODE_READ_BYTE =>
                        COMData <= sign_extend(to_std_logic(comReadyRead/=comLastRead)& ((not COMTransmitBusy) and (comReadySend xor comLastSend)));
                    when RAMMODE_READ_BYTEU =>
                        COMData <= zero_extend(to_std_logic(comReadyRead/=comLastRead)& ((not COMTransmitBusy)  and (comReadySend xor comLastSend)));
                    when others =>
                end case;
            end if;
        elsif(IFDevice = dCOM) then
            -- error
        else
            COMdata <= Zero32;
        end if;
    end process;

    process(all) -- RAM & TLB
    begin
        if(rst = RstEnable) then
            ramMode <= RAMMODE_NOP;
            ramAddr <= Zero32;
            ExpMiss <= '0';
            TLBvaddr <= Zero32;
        elsif(MEMDevice = dTLB) then
            TLBvaddr <= MEMaddr;
            ramAddr <= TLBpaddr;
            if(TLBMiss = '0') then
                ramMode <= MEMMode;
            else
                ramMode <= RAMMODE_NOP;
            end if;
            ExpMiss <= TLBMiss;
        elsif(MEMDevice = dRAM) then
            ExpMiss <= '0';
            TLBvaddr <= Zero32;
            ramAddr <= MEMaddr;
            ramMode <= MEMMode;
        elsif(IFDevice = dTLB) then
            TLBvaddr <= IFpc;
            ramAddr <= TLBpaddr;
            if(TLBMiss = '0') then
                ramMode <= RAMMODE_READ_WORD;
            else
                ramMode <= RAMMODE_NOP;
            end if;
            ExpMiss <= TLBMiss;
        elsif(IFDevice = dRAM) then
            ExpMiss <= '0';
            TLBvaddr <= Zero32;
            ramAddr <= IFpc;
            ramMode <= RAMMODE_READ_WORD;
        else
            ramAddr <= Zero32;
            TLBvaddr <= Zero32;
            ExpMiss <= '0';
            ramMode <= RAMMODE_NOP;
        end if;
    end process;

    process(all) -- Flash
    begin
        if(rst = RstEnable) then
            FlashRe <= '0';
            FlashAddr <= Zero32;
        elsif(MEMDevice = dFlash) then
            FlashAddr <= MEMaddr;
            FlashRe <= '1';
        elsif(IFDevice = dFlash) then
            FlashAddr <= IFpc;
            FlashRe <= '1';
        else
            FlashRe <= '0';
            FlashAddr <= Zero32;
        end if;
    end process;
--    process(all)
--    begin
--        if(rst = RstEnable) then
--            comLastRead <= '1';
--            comLastSend <= '1';
--            IFstall <= '0';
--            ramMode <= RAMMODE_READ_WORD;
--            MEMReadData <= Zero32;
--            ramAddr <= IFpc;
--            ramWdata <= MEMWritedata;
--            ROMaddr <= x"00";
--            ExpMiss <= '0';
--        else
--            IFstall <= '0';
--            ExpMiss <= '0';
--            ramMode <= RAMMODE_READ_WORD;
--            MEMReadData <= Zero32;
--            ramAddr <= IFpc;
--            ramWdata <= MEMWritedata;
--            ROMaddr <= MEMaddr(9 downto 2);
--            if(MEMaddr(31 downto 24) = x"80") then       -- RAM
--                if(MEMMode /= RAMMODE_NOP) then
--                    IFstall <= '1';
--                    ramAddr <= MEMaddr;
--                    ramMode <= MEMMode;
--                    MEMReadData <= ramRdata;
--                end if;
--            elsif(MEMaddr(31 downto 24) = x"BE") then    -- FLASH
--                -- TODO flash
--            elsif(MEMaddr(31 downto 12) = x"BFC00") then -- ROM
--                MEMReadData <= ROMdata;
--            elsif(MEMaddr = x"BFD003F8") then            -- COM data
--                case MEMMode is
--                    when RAMMODE_READ_BYTE =>
--                        MEMReadData <= sign_extend(comRdata);
--                        if(comReadyRead /= comLastRead) then
--                            comLastRead <= comReadyRead;
--                        else
--                            -- error
--                        end if;
--                    when RAMMODE_READ_BYTEU =>
--                        MEMReadData <= zero_extend(comRdata);
--                        if(comReadyRead /= comLastRead) then
--                            comLastRead <= comReadyRead;
--                        else
--                                                    -- error
--                        end if;
--                    when RAMMODE_WRITE_BYTE =>
--                        if(COMTransmitBusy = '0' and (comLastSend /= comReadySend)) then
--                            COMTransmitData <= MEMWritedata(7 downto 0);
--                            comLastSend <= comReadySend;
--                        else
--                            --error
--                        end if;
--                    when others =>  --error

--                end case;
--            elsif (MEMaddr = x"BFD003FC") then           -- COM data
--                case MEMMode is
--                    when RAMMODE_READ_BYTE =>
--                        MEMReadData <= sign_extend(to_std_logic(comReadyRead/=comLastRead)& ((not COMTransmitBusy) and (comReadySend xor comLastSend)));
--                    when RAMMODE_READ_BYTEU =>
--                        MEMReadData <= zero_extend(to_std_logic(comReadyRead/=comLastRead)& ((not COMTransmitBusy)  and (comReadySend xor comLastSend)));
--                    when others =>
--                end case;
--            else                                          -- TLB
--                if(MEMMode /= RAMMODE_NOP) then
--                    IFstall <= '1';
--                    ramAddr <= TLBpaddr;
--                    if(TLBmiss = '0') then
--                        ramMode <= MEMMode;
--                    else
--                        ramMode <= RAMMODE_NOP;
--                    end if;
--                    MEMReadData <= ramRdata;
--                    ExpMiss <= TLBmiss;
--                end if;
--            end if;
--        end if;
--    end process;

    process(all)
    begin
        if (rst = RstEnable) then
            comReadyRead <= '0';
            comRdata <= x"00";
        elsif(rising_edge(COMReceiveReady)) then
            comRdata <= COMReceiveData;
            if(comReadyRead = comLastRead) then  -- this should always happen, otherwise a data is ignored.
                comReadyRead <= not comReadyRead;
            end if;
        end if;
    end process;

    process(all)
    begin
        if (rst = RstEnable) then
            COMTransmitStart1 <= '0';
            comReadySend <= '0';
        elsif ((comReadySend = comLastSend) and (COMTransmitStart1 = '0')) then
            COMTransmitStart1 <= '1';
        elsif(rising_edge(COMTransmitBusy)) then
            comReadySend <= not comLastSend;
            COMTransmitStart1 <= '0';
        end if;
    end process;

end BehavMMU;
