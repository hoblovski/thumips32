----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/12/03 14:29:09
-- Design Name: 
-- Module Name: TLB - BehavTLB
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.consts.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TLB is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           CP0Index: in std_logic_vector(31 downto 0);
           CP0Random: in std_logic_vector(31 downto 0);
           CP0EntryLo0: in std_logic_vector(31 downto 0);
           CP0EntryLo1: in std_logic_vector(31 downto 0);
           CP0EntryHi: in std_logic_vector(31 downto 0);
           we: in std_logic;
           TLBrandom: in std_logic;
           vaddr : in STD_LOGIC_VECTOR (31 downto 0);
           paddr : out STD_LOGIC_VECTOR (31 downto 0);
           watchTLB: out std_logic_vector(63 downto 0);
           TLBmiss : out STD_LOGIC);
end TLB;

architecture BehavTLB of TLB is
    type TLBpages is array(7 downto 0) of std_logic_vector(63 downto 0);
    signal TLB: TLBpages;
begin
    watchTLB <= TLB(1);

    process(clk, rst)
    begin
        if(rst = RstEnable) then
            for i in 0 to 7 loop
                TLB(i) <= Zero32 & Zero32;
            end loop;
        elsif(rising_edge(clk)) then
            if(we = WriteEnable) then
                if(TLBrandom = '0') then  -- tlbwi
                    TLB(to_integer(unsigned(CP0Index(2 downto 0)))) <= "0" & CP0EntryHi(31 downto 13) & CP0EntryLo0(25 downto 6) 
                        & CP0EntryLo0(2) & CP0EntryLo0(1) & CP0EntryLo1(25 downto 6) & CP0EntryLo1(2) & CP0EntryLo1(1);
                else                     -- tlbwr
                    TLB(to_integer(unsigned(CP0Random(2 downto 0)))) <= "0" & CP0EntryHi(31 downto 13) & CP0EntryLo0(25 downto 6) 
                        & CP0EntryLo0(2) & CP0EntryLo0(1) & CP0EntryLo1(25 downto 6) & CP0EntryLo1(2) & CP0EntryLo1(1);
                end if;
            end if;
        end if;
    end process;
    
    process(all)
    begin
        paddr <= Zero32;
        TLBmiss <= '1';
        for i in 0 to 7 loop
            if(TLB(i)(62 downto 44) = vaddr(31 downto 13)) then
                if(vaddr(12) = '0' and TLB(i)(22) = '1') then
                    TLBmiss <= '0';
                    paddr <= TLB(i)(43 downto 24) & vaddr(11 downto 0);
                elsif(vaddr(12) = '1' and TLB(i)(0) = '1') then
                    TLBmiss <= '0';
                    paddr <= TLB(i)(21 downto 2) & vaddr(11 downto 0);
                end if;
            end if;
        end loop;
    end process;

end BehavTLB;
