library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package Consts is
	--------- Enables and Disables -------------

	constant RstEnable: STD_LOGIC := '1';
	constant RstDisable: STD_LOGIC := '0';
	--Rst

	constant WriteEnable: STD_LOGIC := '1';
	constant WriteDisable: STD_LOGIC := '0';
	--Write

	constant ReadEnable: STD_LOGIC := '1';
	constant ReadDisable: STD_LOGIC := '0';
	--Read

	constant ChipEnable: STD_LOGIC := '1';
	constant ChipDisable: STD_LOGIC := '0';
	--Chip

	--------- Enables and Disables -------------

	--------- Valids and Invalids --------------
	constant InstValid: STD_LOGIC := '0';
	constant InstInvalid: STD_LOGIC := '1';
	
	constant Branch: STD_LOGIC := '1';
	constant NotBranch: STD_LOGIC := '0';
	
	constant InDelaySlot: STD_LOGIC := '1';
	constant NotInDelaySlot: STD_LOGIC := '0';
	--------- Valids and Invalids --------------

	--------- Constants ------------------------
	constant Zero32: STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
	constant Zero16: STD_LOGIC_VECTOR(15 downto 0) := x"0000";
	constant TrueLogic: STD_LOGIC := '1';
	constant FalseLogic: STD_LOGIC := '0';
	--------- Constants ------------------------

	--------- Op Codes -------------------------
	constant OpCodeWidth: integer := 6;
	subtype OpCode is STD_LOGIC_VECTOR(OpCodeWidth - 1 downto 0);
	constant Op1CodeWidth: integer := 5;
	subtype Op1Code is STD_LOGIC_VECTOR(Op1CodeWidth - 1 downto 0);
	constant Op2CodeWidth: integer := 6;
	subtype Op2Code is STD_LOGIC_VECTOR(Op2CodeWidth - 1 downto 0);
	constant Op3CodeWidth: integer := 5;
	subtype Op3Code is STD_LOGIC_VECTOR(Op3CodeWidth - 1 downto 0);
	constant InstructionWidth: integer := 32;
	subtype Instruction is STD_LOGIC_VECTOR(InstructionWidth - 1 downto 0);
    --Special
	constant OP_SPECIAL: Opcode := "000000";
	-- Logic
	constant Op_ori: OpCode := "001101";
	constant Op_andi: OpCode := "001100";
	constant Op_xori: OpCode := "001110";
	constant Op_lui: OpCode := "001111";
	constant Op2_or: OpCode := "100101";
	constant Op2_and: OpCode := "100100";
	constant Op2_xor: OpCode := "100110";
	constant Op2_nor: OpCode := "100111";
	-- Shift
	constant Op2_sll: Op2Code := "000000";
	constant Op2_sllv: Op2Code := "000100";
	constant Op2_srl: Op2Code := "000010";
	constant Op2_srlv: Op2Code := "000110";
	constant Op2_sra: Op2Code := "000011";
	constant Op2_srav: Op2Code := "000111";
	-- Move
	constant Op2_MFHI: Op2Code := "010000";
	constant Op2_MFLO: Op2Code := "010010";
	constant Op2_MTHI: Op2Code := "010001";
	constant Op2_MTLO: Op2Code := "010011";
	-- Arithmetic
	constant Op_addiu: Opcode := "001001";
	constant Op_slti: Opcode := "001010";
	constant Op_sltiu: Opcode := "001011";
	constant Op2_ADDU: Op2Code := "100001";
	constant Op2_SUBU: Op2Code := "100011";
	constant Op2_SLTU: Op2Code := "101011";
	constant Op2_SLT: Op2Code := "101010";
	constant Op2_MULT: Op2Code := "011000";
	-- Branch/Jump
	constant Op_beq: OpCode := "000100";
	constant Op_bgez_bltz: OpCode := "000001";
	constant Op3_bgez: Op3Code := "00001";
	constant Op3_bltz: Op3Code := "00000";
	constant Op_bgtz: OpCode := "000111";
	constant Op_blez: OpCode := "000110";
	constant Op_bne: OpCode := "000101";
	constant Op_j: OpCode := "000010";
	constant Op_jal: OpCode := "000011";
	constant Op2_jalr: Op2Code := "001001";
	constant Op2_jr: Op2Code := "001000";
	
	-- Load/Store
    constant OP_LW: OpCode := "100011";
    constant OP_LB: OpCode := "100000";
    constant OP_LBU: OpCode := "100100";
    constant OP_SW: OpCode := "101011";
    constant OP_SB: OpCode := "101000";

	-- Others
	constant Op_C0: OpCode := "010000";
	constant Op1_C0: Op1Code := "00000";
	constant Op2_C0: Op2Code := "000000";		-- in OpenMips, sel(inst(2 downto 0)) is "000"
	constant OpC0CodeWidth: integer := 5;
	subtype OpC0Code is std_logic_vector(OpC0CodeWidth - 1 downto 0);
	constant OpC0_MTC0: OpC0Code := "00100";
	constant OpC0_MFC0: OpC0Code := "00000";
	
	constant Op2_syscall: Op2Code := "001100";
	constant Inst_eret: Instruction := "01000010000000000000000000011000";
	constant Inst_tlbwi: Instruction := "01000010000000000000000000000010";
	constant Inst_tlbwr: Instruction := "01000010000000000000000000000110";
	
	constant Op_nop: OpCode := "000000";
	--------- Op Codes -------------------------

	--------- ALU Operation Codes --------------
	constant ALUOpCodeWidth: integer := 8;
	subtype ALUOpCode is STD_LOGIC_VECTOR(ALUOpCodeWidth - 1 downto 0);
	-- Logic
	constant ALUOp_or: ALUOpCode := "00100001";
	constant ALUOp_and: ALUOpCode := "00100010";
	constant ALUOp_xor: ALUOpCode := "00100011";
	constant ALUOp_nor: ALUOpCode := "00100100";
	-- Shift
	constant ALUOp_sll: ALUOpCode := "10000001";
	constant ALUOp_srl: ALUOpCode := "10000011";
	constant ALUOp_sra: ALUOpCode := "10000101";
	-- Move
	constant ALUOP_MFC0: ALUOpCode := "01100000";
	constant ALUOP_MTC0: ALUOpCode := "01100001";
	constant ALUOP_MFHI: ALUOpCode  := "01100100";
	constant ALUOP_MFLO: ALUOpCode  := "01100101";
	constant ALUOP_MTHI: ALUOpCode  := "01100110";
	constant ALUOP_MTLO: ALUOpCode  := "01100111";
	-- Arithmetic
    constant ALUOp_add: ALUOpCode   := "01000000";
    constant ALUOp_cmp: ALUOpCode   := "01000001";
    constant ALUOp_cmpu: ALUOpCode  := "01000010";
    constant ALUOP_SUB: ALUOpCode   := "01000011";
    constant ALUOP_MULT: ALUOpCode  := "01001000";
	-- Branch/Jump
	constant ALUOp_jr: ALUOpCode := "10100001";
	constant ALUOp_jalr: ALUOpCode := "10100010";
	constant ALUOp_j: ALUOpCode := "10100011";
	constant ALUOp_jal: ALUOpCode := "10100100";
	constant ALUOp_beq: ALUOpCode := "10100101";
	constant ALUOp_bgtz: ALUOpCode := "10100110";
	constant ALUOp_blez: ALUOpCode := "10100111";
	constant ALUOp_bne: ALUOpCode := "10101000";
	constant ALUOp_bgez: ALUOpCode := "10101001";
	constant ALUOp_bltz: ALUOpCode := "10101010";
	-- Load/Store
    constant ALUOP_SW: AluOpCode := "11000000";
    constant ALUOP_SB: AluOpCode := "11000001";
    constant ALUOP_LW: AluOpCode := "11000010";
    constant ALUOP_LB: AluOpCode := "11000011";
    constant ALUOP_LBU: AluOpCode := "11000100";
	-- Others
	constant ALUOp_nop: ALUOpCode := "00000000";
	constant ALUOp_syscall: ALUOpCode := "00000001";
	constant ALUOp_eret: ALUOpCode := "00000010";

	constant ALUSelectCodeWidth: integer := 3;
	subtype ALUSelectCode is STD_LOGIC_VECTOR(ALUSelectCodeWidth - 1 downto 0);
	constant ALUSelect_nop: ALUSelectCode := "000";
	constant ALUSelect_logic: ALUSelectCode := "001";
    constant ALUSelect_arith: ALUSelectCode := "010";
    constant ALUSelect_move: ALUSelectCode := "011";
    constant ALUSelect_shift: ALUSelectCode := "100";
    constant ALUSelect_branchjump: ALUSelectCode := "101";
	--------- ALU Operation Codes --------------

	--------- ROM Defines ----------------------
	--这部分目前是抄的
    subtype DWord is std_logic_vector(31 downto 0);
    subtype Byte is std_logic_vector(7 downto 0);
	constant InstAddrBus: integer := 32;
	constant InstBus: integer := 32;
	constant InstMemNum: integer := 256;
	constant InstMemNumLog2: integer := 17;
	--------- ROM Defines ----------------------

    --------- MEM Defines ----------------------
    constant DATAMEM_SZ_B: integer := 1048576;
    subtype RANGE_BYTE0 is Natural range 7 downto 0;
    subtype RANGE_BYTE1 is Natural range 15 downto 8;
    subtype RANGE_BYTE2 is Natural range 23 downto 16;
    subtype RANGE_BYTE3 is Natural range 31 downto 24;
    -- at any time, no more than 1 of the 5 insts will access memory
    -- thus read and write cannot uccur at the same time
    type T_RAMMode is (
            RAMMODE_NOP,               
            RAMMODE_READ_WORD,
            RAMMODE_READ_BYTE,
            RAMMODE_READ_BYTEU,
            RAMMODE_WRITE_BYTE,       
            RAMMODE_WRITE_WORD       
    );
    constant LED_MEMADDR: DWord := x"00010000";
    --------- MEM Defines ----------------------

	--------- Register Defines -----------------
	constant RegNum: integer := 32;
	constant RegWidth: integer := 32;
	constant DoubleRegWidth: integer := 64;
	constant RegAddrBus: integer := 5;
	constant RegNumLog2: integer := 5;
	--------- Register Defines -----------------

    --------- Instruction Encoding Scheme Ranges ---------
    subtype RANGE_OPCODE is Natural range 31 downto 26;
    subtype RANGE_RS is Natural range 25 downto 21;
    subtype RANGE_RT is Natural range 20 downto 16;
    subtype RANGE_RD is Natural range 15 downto 11;
    subtype RANGE_SA is Natural range 10 downto 6;
    subtype RANGE_SP is Natural range 5 downto 0;
    subtype RANGE_IMM is Natural range 15 downto 0;
    --------- Instruction Encoding Scheme Ranges ---------

	--------- CP0 Defines -----------
	-- Register Addresses
	subtype CP0RegAddr is STD_LOGIC_VECTOR(RegAddrBus - 1 downto 0);
	constant CP0Reg_Index: CP0RegAddr := "00000";
	constant CP0Reg_Random: CP0RegAddr := "00001";
	constant CP0Reg_EntryLo0: CP0RegAddr := "00010";
	constant CP0Reg_EntryLo1: CP0RegAddr := "00011";
	constant CP0Reg_Wired: CP0RegAddr := "00110";
	constant CP0Reg_BadVAddr: CP0RegAddr := "01000";
	constant CP0Reg_Count: CP0RegAddr := "01001";
	constant CP0Reg_EntryHi: CP0RegAddr := "01010";
	constant CP0Reg_Compare: CP0RegAddr := "01011";
	constant CP0Reg_Status: CP0RegAddr := "01100";
	constant CP0Reg_Cause: CP0RegAddr := "01101";
	constant CP0Reg_Epc: CP0RegAddr := "01110";
	constant CP0Reg_Prid: CP0RegAddr := "01111";
	constant CP0Reg_Config: CP0RegAddr := "10000";
	
	-- Interrupt Status
	constant InterruptAssert: STD_LOGIC := '1';
	constant InterruptNotAssert: STD_LOGIC := '0';
	--------- CP0 Defines -----------
	
	--------- Exceptions Defines -----------
    subtype ExceptionType is STD_LOGIC_VECTOR(31 downto 0);
    constant Except_Interrupt: ExceptionType := x"00000001";
    constant Except_Syscall: ExceptionType := x"00000008";
    constant Except_InstInvalid: ExceptionType := x"0000000a";
    constant Except_Eret: ExceptionType := x"0000000e";
    constant Except_TLBload: ExceptionType := x"00000002";
    constant Except_TLBstore: ExceptionType := x"00000003";
	--------- Exceptions Defines -----------
		
    --------- TLB Defines ------------------
    constant TLBnum: Dword := x"00000007";
    constant TLBfix: Dword := x"00000002";
    
	--------- TLB Defines ------------------
		
    ----------- functions -----------
    -- Returns: 32-bit, sign extended of x
    --      *** undefined result on x(l to h) instead of (h downto l) ***
    function sign_extend(x: std_logic_vector)
        return std_logic_vector;
    function zero_extend(x: std_logic_vector)
        return std_logic_vector;
    -- Returns 2's complementary code
    function comp_code(x: std_logic_vector)
        return std_logic_vector;
    function to_std_logic(x: boolean) 
        return std_logic;
    ----------- functions -----------

end package;

package body consts is

    function zero_extend(x: std_logic_vector)
        return std_logic_vector
    is
        variable rv: std_logic_vector(31 downto 0);
    begin
        rv(x'length - 1 downto 0) := x;
        rv(rv'length - 1 downto x'length) := (others=> '0');
        return rv;
    end zero_extend;

    function sign_extend(x: std_logic_vector)
        return std_logic_vector
    is
        variable rv: std_logic_vector(31 downto 0);
    begin
        rv(x'length - 1 downto 0) := x;
        rv(rv'length - 1 downto x'length) := (others=> x(x'high));
        return rv;
    end sign_extend;

    function comp_code(x: std_logic_vector)
        return std_logic_vector
    is
        variable rv: std_logic_vector(31 downto 0);
        constant mask: std_logic_vector(31 downto 0) := (others=> '1');
    begin
        rv := mask xor x;
        rv := std_logic_vector(unsigned(rv) + 1);
        return rv;
    end;
    
    function to_std_logic(x: boolean)
        return std_logic
    is
    begin
        if(x) then
            return '1';
        else 
            return '0';
        end if;
    end;

end consts;
