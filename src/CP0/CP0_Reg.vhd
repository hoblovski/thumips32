----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/23 19:13:12
-- Design Name: 
-- Module Name: CP0_Reg - Behav_CP0_Reg
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CP0_Reg is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           raddr : in STD_LOGIC_VECTOR (4 downto 0);
           int : in STD_LOGIC_VECTOR (5 downto 0);
           we : in STD_LOGIC;
           waddr : in STD_LOGIC_VECTOR (4 downto 0);
           wdata : in STD_LOGIC_VECTOR (31 downto 0);
           rdata : out STD_LOGIC_VECTOR (31 downto 0);
           index : out std_logic_vector(31 downto 0);
           random : out std_logic_vector(31 downto 0);
           entrylo0 : out std_logic_vector(31 downto 0);
           entrylo1 : out std_logic_vector(31 downto 0);
           entryhi : out std_logic_vector(31 downto 0);
           badvaddr : out std_logic_vector(31 downto 0);
           count : out STD_LOGIC_VECTOR (31 downto 0);
           compare : out STD_LOGIC_VECTOR (31 downto 0);
           status : out STD_LOGIC_VECTOR (31 downto 0);
           cause : out STD_LOGIC_VECTOR (31 downto 0);
           epc : out STD_LOGIC_VECTOR (31 downto 0);
           config : out STD_LOGIC_VECTOR (31 downto 0);
           prid : out STD_LOGIC_VECTOR (31 downto 0);
           timer_int : out STD_LOGIC;
           -- exceptions
           exceptType: in STD_LOGIC_VECTOR(31 downto 0);
           currentInstAddr: in STD_LOGIC_VECTOR(31 downto 0);
           isInDelaySlot: in STD_LOGIC;
           -- TLBmiss
           TLBmiss: in std_logic;
           vaddr: in std_logic_vector(31 downto 0)
           );
end CP0_Reg;

architecture Behav_CP0_Reg of CP0_Reg is
	signal count1: STD_LOGIC_VECTOR (31 downto 0);
	signal compare1: STD_LOGIC_VECTOR (31 downto 0);
	signal status1: STD_LOGIC_VECTOR (31 downto 0);
	signal epc1: STD_LOGIC_VECTOR (31 downto 0);
	signal cause1: STD_LOGIC_VECTOR (31 downto 0);
	signal prid1: STD_LOGIC_VECTOR (31 downto 0);
	signal config1: STD_LOGIC_VECTOR (31 downto 0);
	signal index1: std_logic_vector(31 downto 0);
	signal random1: std_logic_vector(31 downto 0);
	signal entrylo01: std_logic_vector(31 downto 0);
	signal entrylo11: std_logic_vector(31 downto 0);
	signal entryhi1: std_logic_vector(31 downto 0);
	signal wired1: std_logic_vector(31 downto 0);
	signal badvaddr1: std_logic_vector(31 downto 0);
begin
	count <= count1;
	compare <= compare1;
	status <= status1;
	epc <= epc1;
	cause <= cause1;
	prid <= prid1;
	config <= config1;
	index <= index1;
	random <= random1;
	entrylo0 <= entrylo01;
	entrylo1 <= entrylo11;
	entryhi <= entryhi1;
	badvaddr <= badvaddr1;
	-------- Write to CP0 Registers ---------
	process (all)
	begin
		if (rising_edge (clk)) then
			if (rst = RstEnable) then
			    index1 <= Zero32;
			    random1 <= TLBnum;
			    entrylo01 <= Zero32;
			    entrylo11 <= Zero32;
			    entryhi1 <= Zero32;
			    badvaddr1 <= Zero32;
			    wired1 <= TLBfix;
				count1 <= Zero32;
				compare1 <= Zero32;
				status1 <= x"10000000";
				cause1 <= Zero32;
				epc1 <= Zero32;
				config1 <=  x"00008000";
				prid1 <= x"004C0102";
				timer_int <= InterruptNotAssert;
			else
				count1 <= std_logic_vector(unsigned(count1) + 1);
				if(random1 = wired1 or random1 = Zero32) then
				    random1 <= TLBnum;
				else
				    random1 <= std_logic_vector(unsigned(random1) - 1);
				end if;
				cause1(15 downto 10) <= int;
				
				if(TLBmiss = '1') then
				    badvaddr1 <= vaddr;
                end if;
				
				if (compare1 /= Zero32 and count1 = compare1) then
					timer_int <= InterruptAssert;
				end if;
				
				if (we = WriteEnable) then
					case (waddr) is
				        when CP0Reg_Index =>
				            index1 <= wdata;
				        when CP0Reg_EntryLo0 =>
				            entrylo01 <= wdata;
				        when CP0Reg_EntryLo1 =>
				            entrylo11 <= wdata;
                        when CP0Reg_Wired =>
                            wired1 <= wdata;
                        when CP0Reg_EntryHi =>
                            entryhi1 <= wdata;
						when CP0Reg_Count =>
							count1 <= wdata;
						when CP0Reg_Compare =>
							compare1 <= wdata;
							timer_int <= InterruptNotAssert;
						when CP0Reg_Status =>
							status1 <= wdata;
						when CP0Reg_Epc =>
							epc1 <= wdata;
						when CP0Reg_Cause =>
							cause1(9 downto 8) <= wdata(9 downto 8);
							cause1(23) <= wdata(23); 
							cause1(22) <= wdata(22);
						when others =>
					end case;
				end if;
				case (exceptType) is
				    when Except_Interrupt =>
				        if (isInDelaySlot = InDelaySlot) then
				            epc1 <= std_logic_vector(unsigned(currentInstAddr) - 4);
				            cause1(31) <= '1';
				        else   
				            epc1 <= currentInstAddr;
				            cause1(31) <= '0';
				        end if;
				        status1(1) <= '1';
				        cause1(6 downto 2) <= "00000";
				    when Except_syscall =>
				        if (status1(1) = '0') then
				            if (isInDelaySlot = InDelaySlot) then
				                epc1 <= std_logic_vector(unsigned(currentInstAddr) - 4);
				                cause1(31) <= '1';
				            else 
				                epc1 <= currentInstAddr;
				                cause1(31) <= '0';
				            end if;
				        end if;
				        status1(1) <= '1';
				        cause1(6 downto 2) <= "01000";
				    when Except_InstInvalid =>
				        if(status(1) = '0') then
                            if (isInDelaySlot = InDelaySlot) then
                                epc1 <= std_logic_vector(unsigned(currentInstAddr) - 4);
                                cause1(31) <= '1';
                            else 
                                epc1 <= currentInstAddr;
                                cause1(31) <= '0';
                            end if;
                        end if;
                        status1(1) <= '1';
                        cause1(6 downto 2) <= "01010";
                    when Except_Eret =>
                        status1(1) <= '0';
                    when Except_TLBload =>
                        if(status(1) = '0') then
                            if (isInDelaySlot = InDelaySlot) then
                                epc1 <= std_logic_vector(unsigned(currentInstAddr) - 4);
                                cause1(31) <= '1';
                            else 
                                epc1 <= currentInstAddr;
                                cause1(31) <= '0';
                            end if;
                        end if;
                        status1(1) <= '1';
                        cause1(6 downto 2) <= "00010";
                    when Except_TLBstore =>
                        if(status(1) = '0') then
                            if (isInDelaySlot = InDelaySlot) then
                                epc1 <= std_logic_vector(unsigned(currentInstAddr) - 4);
                                cause1(31) <= '1';
                            else 
                                epc1 <= currentInstAddr;
                                cause1(31) <= '0';
                            end if;
                        end if;
                        status1(1) <= '1';
                        cause1(6 downto 2) <= "00011";
                    
				    when others =>
				 end case;
			end if;
		end if;
	end process;
	-------- Write to CP0 Registers ---------
	
	-------- Read from CP0 Registers ---------
	process (all)
	begin
		if (rst = RstEnable) then
			rdata <= Zero32;
		else
			case (raddr) is
			    when CP0Reg_Index =>
			        rdata <= index1;
                when CP0Reg_Random =>
                    rdata <= random1;
                when CP0Reg_EntryLo0 =>
                    rdata <= entrylo01;
                when CP0Reg_EntryLo1 =>
                    rdata <= entrylo11;
                when CP0Reg_Wired =>
                    rdata <= wired1;
                when CP0Reg_EntryHi =>
                    rdata <= entryhi1;
                when CP0Reg_BadVAddr =>
                    rdata <= badvaddr1;
				when CP0Reg_Count =>
					rdata <= count1;
				when CP0Reg_Compare =>
					rdata <= compare1;
				when CP0Reg_Status =>
					rdata <= status1;
				when CP0Reg_Cause =>
					rdata <= cause1;
				when CP0Reg_Epc =>
					rdata <= epc1;
				when CP0Reg_Prid =>
					rdata <= prid1;
				when CP0Reg_Config =>
					rdata <= config1;
				when others =>
                    rdata <= Zero32;
			end case;
		end if;
	end process;           
	-------- Read from CP0 Registers ---------
end Behav_CP0_Reg;
