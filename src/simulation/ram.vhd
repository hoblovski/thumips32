----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 19:06:58
-- Design Name: 
-- Module Name: Inst_RAM - BehaviINSTRAM
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Consts.ALL;
use IEEE.std_logic_textio.ALL;
use STD.textio.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM is
    generic (
        infile: in string);
    Port ( 
        clk: in std_logic;
        rst: in std_logic;
        addr: in DWord;
        rdata: out DWord;
        wdata: in DWord;
        mode: in T_RAMMode
    );
end RAM;

architecture BehavRAM of RAM is
    type T_Mem is array(0 to DATAMEM_SZ_B) of Byte;
    -- *** no assumption about power-on memory reset ***
    signal mem: T_Mem;
begin

    -- write (happens on clk rising edges) (*** not a practical assump. ***)
    -- do not consider forwarding.
    -- because for the 5 insts. in pipeline,
    -- at most one of them is accessing the memory
    process (rst, clk)
        file InputFile: text;
        variable Iline: line;
        variable i : integer;
        variable tmp: std_logic_vector(31 downto 0);

        variable ad0: integer;

    begin
        if (rst = '1') then
            i := 0;
            file_open(InputFile, infile, read_mode);
            while not endfile(InputFile) loop
                if (i mod 1000 = 0) then
                    report "init ram, at byte " & integer'image(i);
                end if;
                readline(InputFile, Iline);
                hread(Iline, tmp); -- Hexadecimal
                --report "MEM(" & integer'image(i) & ");   " & "VALUE: " & integer'image(to_integer(unsigned(tmp(7 downto 0))));
                mem(i)     <= tmp(7 downto 0);
                --report "MEM(" & integer'image(i+1) & ");   " & "VALUE: " & integer'image(to_integer(unsigned(tmp(15 downto 8))));
                mem(i + 1) <= tmp(15 downto 8);
                --report "MEM(" & integer'image(i+2) & ");   " & "VALUE: " & integer'image(to_integer(unsigned(tmp(23 downto 16))));
                mem(i + 2) <= tmp(23 downto 16);
                --report "MEM(" & integer'image(i+3) & ");   " & "VALUE: " & integer'image(to_integer(unsigned(tmp(31 downto 24))));
                mem(i + 3) <= tmp(31 downto 24);
                i := i + 4;
            end loop;
        elsif (rising_edge(clk)) then  
            ad0 := to_integer(unsigned(addr(30 downto 0)));
            case mode is
                when RAMMODE_WRITE_WORD =>
                    mem(ad0 + 3) <= wdata(RANGE_BYTE3);
                    mem(ad0 + 2) <= wdata(RANGE_BYTE2);
                    mem(ad0 + 1) <= wdata(RANGE_BYTE1);
                    mem(ad0) <= wdata(RANGE_BYTE0);
                when RAMMODE_WRITE_BYTE =>
                    mem(ad0) <= wdata(RANGE_BYTE0);
                when others => 
                    null;
            end case;
        end if;
    end process;

    -- read is combinational logic
    process (all)
        variable ad0: integer;
        variable memad0: Byte;
    begin
        ad0 := to_integer(unsigned(addr(30 downto 0)));
        case mode is
            when RAMMODE_READ_WORD =>
                rdata <= mem(ad0 + 3)
                         & mem(ad0 + 2)
                         & mem(ad0 + 1)
                         & mem(ad0);
            when RAMMODE_READ_BYTE =>
                rdata <= sign_extend(mem(ad0));
            when RAMMODE_READ_BYTEU =>
                rdata <= zero_extend(mem(ad0));
            when others=>
                rdata <= (others=> 'Z');
        end case;
    end process;

end BehavRAM;

