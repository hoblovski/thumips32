----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 2017/10/14 19:28:22
-- Design Name: 
-- Module Name: testbench - Test
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testbench is
--  Port ( );
end testbench;

architecture Test of testbench is
    signal CLOCK_50MHz: std_logic;
    signal rst: std_logic;
    signal int: std_logic_vector(5 downto 0);
    signal timer_int: std_logic;
    constant infile: string := "../../../Test_Files/ins.out.txt";
    constant stdfile: string := " ";
begin
    -- Generate CLOCK_50MHz
    process
    begin
        CLOCK_50MHz <= '0';
        while true loop
            wait for 10 ns;
            CLOCK_50MHz <= not CLOCK_50MHz;
        end loop;
    end process;

    -- Generate rst
    process
    begin
        rst <= '1';
        wait for 15 ns;
        rst <= '0';
        wait;
    end process;
    
    -- Generate int
    process (timer_int)
    begin
        int <= "00000" & timer_int ;
    end process;

    uSOPC: entity work.SOPC generic map(
        infile => infile,
        stdfile => stdfile)
    port map(
        clk => CLOCK_50MHz,
        rst => rst,
        int => int,
        timer_int =>timer_int);

end Test;
