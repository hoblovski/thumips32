with open("ins.out.txt", "r") as fin, open("ins.bin", "wb") as fout:
    for linenum, line in enumerate(fin):
        l = line.strip()
        if len(l) != 8:
            print("Line %d length error" % linenum)
            exit()
        l = [int(l[6:8], 16), int(l[4:6], 16), int(l[2:4], 16), int(l[0:2], 16)]
        fout.write(bytearray(l))
