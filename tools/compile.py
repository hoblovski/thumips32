# encoding: utf8
"""
用法:
    汇编程序保存在ins.txt中, 之后
    $ python compile.py
    结果输出到ins.out.txt
    加入指令:
        实现一个函数, 
            函数名: 指令名称
            函数参数: ss = s.replace(",", "").split() (指令tokenize的结果)
            函数返回: 对应机器码的字符串
        encReg(s):
            将如 "$5" 转换成 "00101"
        encHexImm(s)
            将如 "0x23" 转换成 "0000000000100011" (其二进制表达, 16位)
"""

def encReg(s):
    return bin(int(s[1:]))[2:].rjust(5, "0")

def encHexImm(s, div=1):
    return bin(int(s[2:], 16) // 1)[2:].rjust(16, "0") 
    
def encHexImmLong(s, div=1):
    return bin(int(s[2:], 16) // 1)[2:].rjust(26, "0") 

def _nop(ss):
    rv = "00000000"
    rv = rv + rv + rv + rv
    return rv

def _beq(ss):
    rv = "000100"
    rv = rv + encReg(ss[1])
    rv = rv + encReg(ss[2])
    rv = rv + encHexImm(ss[3], div=4)
    return rv
    
def _bgez(ss):
    rv = "000001"
    rv = rv + encReg(ss[1])
    rv = rv + "00001"
    rv = rv + encHexImm(ss[2], div=4)
    return rv

def _bgtz(ss):
    rv = "000111"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + encHexImm(ss[2], div=4)
    return rv
    
def _blez(ss):
    rv = "000110"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + encHexImm(ss[2], div=4)
    return rv

def _bltz(ss):
    rv = "000110"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + encHexImm(ss[2], div=4)
    return rv

def _bne(ss):
    rv = "000101"
    rv = rv + encReg(ss[1])
    rv = rv + encReg(ss[2])
    rv = rv + encHexImm(ss[3], div=4)
    return rv  
  
def _b(ss):
    rv = "0001000000000000"
    rv = rv + encHexImm(ss[1])
    return rv
    
def _j(ss):
    rv = "000010"
    rv = rv + encHexImmLong(ss[1], div=4)
    return rv
    
def _jal(ss):
    rv = "000011"
    rv = rv + encHexImmLong(ss[1], div=4)
    return rv
    
def _jalr(ss):
    rv = "000000"
    if ss[2][0] == '$':
        rv = rv + encReg(ss[2])
        rv = rv + "00000"
        rv = rv + encReg(ss[1])
        rv = rv + "00000001001"
    else:
        rv = rv + encReg(ss[1])
        rv = rv + "00000"
        rv = rv + encReg('$31')
        rv = rv + "00000001001"
    return rv

def _jr(ss):
    rv = "000000"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "001000"
    return rv
    
def _ori(ss):
    rv = "001101"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv
    
def _or(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100101"
    return rv

def _andi(ss):
    rv = "001100"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv
    
def _and(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100100"
    return rv
    
def _xor(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100110"
    return rv
    
def _xori(ss):
    rv = "001110"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv
    
def _nor(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100111"
    return rv

def _lui(ss):
    rv = "001111"
    rv = rv + "00000"
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv
    
def _addiu(ss):
    rv = "001001"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv

def _slti(ss):
    rv = "001010"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv

def _sltiu(ss):
    rv = "001011"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[3])
    return rv

def _addu(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100001"
    return rv

def _subu(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "100011"
    return rv

def _sltu(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "101011"
    return rv

def _slt(ss):
    rv = "000000"
    rv = rv + encReg(ss[2])
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "101010"
    return rv

def _mfhi(ss):
    rv = "000000"
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "010000"
    return rv

def _mflo(ss):
    rv = "000000"
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "010010"
    return rv

def _mthi(ss):
    rv = "000000"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "010001"
    return rv

def _mtlo(ss):
    rv = "000000"
    rv = rv + encReg(ss[1])
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "010011"
    return rv

def _mult(ss):
    rv = "000000"
    rv = rv + encReg(ss[1])
    rv = rv + encReg(ss[2])
    rv = rv + "00000"
    rv = rv + "00000"
    rv = rv + "011000"
    return rv
    
def _mtc0(ss):
    rv = "01000000100"
    rv = rv + encReg(ss[1])
    rv = rv + encReg(ss[2])
    rv = rv + "00000000000"
    return rv
    
def _mfc0(ss):
    rv = "01000000000"
    rv = rv + encReg(ss[1])
    rv = rv + encReg(ss[2])
    rv = rv + "00000000000"
    return rv

def _sw(ss):
    rv = "101011"
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv

def _sb(ss):
    rv = "101000"
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv

def _lw(ss):
    rv = "100011"
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv

def _lb(ss):
    rv = "100000"
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv

def _lbu(ss):
    rv = "100100"
    rv = rv + encReg(ss[3])
    rv = rv + encReg(ss[1])
    rv = rv + encHexImm(ss[2])
    return rv
	
def _syscall(ss):
	rv = "00000000000000000000000000001100"
	return rv
	
def _eret(ss):
	rv = "01000010000000000000000000011000"
	return rv

def enc32(s):
    assert len(s) == 32
    rv = ""
    for i in range(0, 32, 4):
        rv = rv + hex(int("0b"+s[i:i+4], 2))[2:]
    return rv

    
    
line = 0
fin = open("ins.txt", "r")
fout = open("ins.out.txt", "w")
def ass(s):
    global line,fin,fout
    try:
        ss = s.replace(",", " ").replace("(", " ").replace(")", " ").split()
        if ss[0] == '.org':
            pl = int(ss[1][2:], 16)
            print ("switch place to ", pl)
            while line < pl:
                fout.write(enc32(_nop(None)) + "\n")
                line += 4
            line -= 4
            return ""
        else:
            c = "_" + ss[0] + "(ss)"
            return enc32(eval(c)) + "\n"
    except e:
        print("Error assembling %s" % s)
        print("Exception message: \n%s" % e)
        exit()

for l in fin:
    if l.strip() == "":
        continue
    s = ass(l)
    fout.write(s)
    line = line + 4
