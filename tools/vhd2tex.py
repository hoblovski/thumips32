# encoding: utf8
import sys
import re

##############################################################################
PRE = r"""
\documentclass{ctexart}

\usepackage{tabularx}
\usepackage{multirow}           % mutlirow

% for simulating booktabs rules; so they work with vertical lines
\newlength{\Oldarrayrulewidth}
\newcommand{\Cline}[2]{
  \noalign{\global\setlength{\Oldarrayrulewidth}{\arrayrulewidth}}
  \noalign{\global\setlength{\arrayrulewidth}{#1}}\cline{#2}
  \noalign{\global\setlength{\arrayrulewidth}{\Oldarrayrulewidth}}}
\newcommand{\Hline}[1]{
  \noalign{\global\setlength{\Oldarrayrulewidth}{\arrayrulewidth}}
  \noalign{\global\setlength{\arrayrulewidth}{#1}}\hline
  \noalign{\global\setlength{\arrayrulewidth}{\Oldarrayrulewidth}}}
\newcommand{\Topline}{\Hline{0.08em}}
\newcommand{\Bottomline}{\Hline{0.08em}}
\newcommand{\Midline}{\Hline{0.05em}}
\newcommand{\CMidLine}[1]{\Cline{0.05em}{#1}}

% for formatting a row
\newcolumntype{+}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}#1\ignorespaces}

% define new stretchable column types
\newcolumntype{L}{>{\raggedright\arraybackslash}X}
\newcolumntype{R}{>{\raggedleft\arraybackslash}X}
\newcolumntype{C}{>{\centering\arraybackslash}X}

\begin{document}
"""
##############################################################################
TEMPLATE = r"""
    \begin{table}[ht!]
    \centering
        \begin{tabularx}{\textwidth}{|c|>{\sffamily}ccC|}
        \multicolumn{4}{c}{\Large <<<MODULE_NAME>>>}
        \\\Topline
        \multicolumn{1}{|c}{} & \mdseries \rmfamily 信号名 & 来源/去往 & 意义\\
        \Midline
        \multirow{<<<N_IN>>>}*{输入}
            <<<IN>>>
        \Midline
        \multirow{<<<N_OUT>>>}*{输出}
            <<<OUT>>>
        \Bottomline
        \end{tabularx}
    \caption{<<<MODULE_NAME>>>的模块说明}
    \end{table}
"""

# sample
INTEST = r"""
            & rst & rst来源 & rst意义\\
            & clk & clk来源 & clk意义\\
            & branchflag & branchflag来源 & branchflag意义\\
            & branchaddr & branchaddr来源 & branchaddr意义\\

"""

# sample out
OUTTEST = r"""
            & pc & pc去向 & pc意义\\
            & ce & ce来源 & ce意义\\
"""

def basic_fill(MODULE_NAME, IN, OUT, N_IN, N_OUT):
    return TEMPLATE\
            .replace("<<<MODULE_NAME>>>", MODULE_NAME)\
            .replace("<<<IN>>>", IN)\
            .replace("<<<OUT>>>", OUT)\
            .replace("<<<N_IN>>>", N_IN)\
            .replace("<<<N_OUT>>>", N_OUT)
##############################################################################
POST = r"""
\end{document}
"""
##############################################################################

glb_n_in = 0
glb_n_out = 0
glb_in = ""
glb_out = ""
glb_module_name = "DefaultModuleName"
glb_output_filename = "output.tex"

def init():
    global glb_n_in, glb_n_out, glb_in, glb_out, glb_module_name
    glb_n_in = 0
    glb_n_out = 0
    glb_in = ""
    glb_out = ""
    glb_module_name = "DefaultModuleName"

def set_module_name(module_name):
    global glb_module_name
    glb_module_name = module_name.replace("_", r"\_")

def add_in(sig_name):
    global glb_in, glb_n_in
    glb_n_in = glb_n_in + 1
    glb_in = glb_in + "\n" +\
            r"& <<<SIG_NAME>>> & 来源 & 意义\\"\
                    .replace("<<<SIG_NAME>>>", sig_name.replace("_", r"\_").rjust(25, " "))

def add_out(sig_name):
    global glb_out, glb_n_out
    glb_n_out = glb_n_out + 1
    glb_out = glb_out + "\n" +\
            r"& <<<SIG_NAME>>> & 去向 & 意义\\"\
                    .replace("<<<SIG_NAME>>>", sig_name.replace("_", r"\_").rjust(25, " "))

def generate():
    with open(glb_output_filename, "w", encoding="utf8") as fout:
        print(PRE, file=fout)
        print(basic_fill(glb_module_name,\
                    "\n"+"%"*78+ glb_in + "\n"+"%"*78+"\n",\
                    "\n"+"%"*78+ glb_out + "\n"+"%"*78+"\n",\
                    str(glb_n_in),\
                    str(glb_n_out)),\
                file=fout)
        print(POST, file=fout)

##############################################################################

ENTITY_PTN = r"""entity\s*(?P<MODULE_NAME>.*?)\s*is\s*Port\s*\((?P<BODY>.*?)\);\s*end\s*(?P<MODULE_NAME1>.*?);"""

BODY_LINE_PTN = r"""\s*(?P<SIG_NAME>\w*)\s*:\s*(?P<DIR>in|out)\s*(?P<TYPE>.*);?"""

ENTITY_PTN = re.compile(ENTITY_PTN, re.MULTILINE | re.IGNORECASE | re.DOTALL)
BODY_LINE_PTN = re.compile(BODY_LINE_PTN, re.IGNORECASE)

def parse(prog):
    t = list(ENTITY_PTN.finditer(prog))
    assert(len(t) == 1)
    print("Got entity declaration")
    ent = t[0]
    module_name = ent.group("MODULE_NAME")
    module_name1 = ent.group("MODULE_NAME1")
    entbody = ent.group("BODY")
    assert (module_name.lower() == module_name1.lower())
    set_module_name(module_name)
    print("Module Name: %s" % module_name)
    print("""
Each time the program prints a signal,
if you press Enter, then the signal is accepted;
if you type n before Enter, then it's rejected.""")
    for l_ in entbody.split("\n"):
        t = list(BODY_LINE_PTN.finditer(l_))
        if (len(t) != 1):
            print("skipping line: %s" % l_)
            continue
        t = t[0]
        sig_name = t.group("SIG_NAME")
        dir_ = t.group("DIR")
        type_ = t.group("TYPE")
        print("% 20s % 10s % 40s" % (sig_name, dir_, type_))
        if (dir_.lower() == "in"):
            add_in(sig_name)
        else:
            add_out(sig_name)

if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print("Usage: python vhd2tex.py INPUT_VHD [OUTPUT_TEX]")
        exit(1)
    filename = sys.argv[1].strip()
    if (len(sys.argv) == 3):
        glb_output_filename = sys.argv[2].strip()
    with open(filename, "r") as fin:
        prog = fin.read()
        parse(prog)
        generate()
        print("Output written to %s" % (glb_output_filename))
        print("Use `xelatex %s` to compile" % (glb_output_filename))


#add_in("clk")
#add_in("rst")
#add_out("pc")
#add_out("inst")
#generate()
