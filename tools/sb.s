
main.elf:     file format elf32-tradlittlemips
main.elf

Disassembly of section .text:

00000000 <_ftext>:
   0:	34010000 	li	at,0x0
   4:	34020000 	li	v0,0x0
   8:	34030000 	li	v1,0x0
   c:	34040000 	li	a0,0x0
  10:	34050000 	li	a1,0x0
  14:	34060000 	li	a2,0x0
  18:	34070000 	li	a3,0x0
  1c:	34080000 	li	t0,0x0
  20:	34090000 	li	t1,0x0
  24:	340a0000 	li	t2,0x0
  28:	340b0000 	li	t3,0x0
  2c:	340c0000 	li	t4,0x0
  30:	340d0000 	li	t5,0x0
  34:	340e0000 	li	t6,0x0
  38:	340f0000 	li	t7,0x0
  3c:	34100000 	li	s0,0x0
  40:	34110000 	li	s1,0x0
  44:	34120000 	li	s2,0x0
  48:	34130000 	li	s3,0x0
  4c:	34140000 	li	s4,0x0
  50:	34150000 	li	s5,0x0
  54:	34160000 	li	s6,0x0
  58:	34170000 	li	s7,0x0
  5c:	34180000 	li	t8,0x0
  60:	34190000 	li	t9,0x0
  64:	341a0000 	li	k0,0x0
  68:	341b0000 	li	k1,0x0
  6c:	341c0000 	li	gp,0x0
  70:	341d0000 	li	sp,0x0
  74:	341e0000 	li	s8,0x0
  78:	341f0000 	li	ra,0x0
  7c:	3c060040 	lui	a2,0x40
  80:	40866000 	mtc0	a2,$12
  84:	40806800 	mtc0	zero,$13
  88:	3c1d0020 	lui	sp,0x20
  8c:	27bd0180 	addiu	sp,sp,384
  90:	3c1c0001 	lui	gp,0x1
  94:	279c81a0 	addiu	gp,gp,-32352
  98:	34070000 	li	a3,0x0
  9c:	00e00013 	mtlo	a3
  a0:	34180000 	li	t8,0x0
  a4:	03000011 	mthi	t8
  a8:	0800002c 	j	b0 <locate>
  ac:	00000000 	nop

000000b0 <locate>:
  b0:	3409ffff 	li	t1,0xffff
  b4:	3c080001 	lui	t0,0x1
  b8:	ad090000 	sw	t1,0(t0)
  bc:	3c110001 	lui	s1,0x1
  c0:	36310010 	ori	s1,s1,0x10
  c4:	3c130000 	lui	s3,0x0
  c8:	ae330000 	sw	s3,0(s1)

000000cc <inst_test>:
  cc:	0c000048 	jal	120 <n52_sb_test>
  d0:	00000000 	nop
  d4:	0c000040 	jal	100 <wait_1s>
  d8:	00000000 	nop

000000dc <test_end>:
  dc:	24040001 	li	a0,1
  e0:	10930003 	beq	a0,s3,f0 <test_end+0x14>
  e4:	00000000 	nop
  e8:	10000003 	b	f8 <test_end+0x1c>
  ec:	00000000 	nop
  f0:	3c080001 	lui	t0,0x1
  f4:	ad000000 	sw	zero,0(t0)
  f8:	1000ffff 	b	f8 <test_end+0x1c>
  fc:	00000000 	nop

00000100 <wait_1s>:
 100:	24080001 	li	t0,1
 104:	2508ffff 	addiu	t0,t0,-1
 108:	1500fffe 	bnez	t0,104 <wait_1s+0x4>
 10c:	00000000 	nop
 110:	03e00008 	jr	ra
 114:	00000000 	nop
	...

00000120 <n52_sb_test>:
 120:	3c043400 	lui	a0,0x3400
 124:	24020000 	li	v0,0
 128:	3c096070 	lui	t1,0x6070
 12c:	352947b4 	ori	t1,t1,0x47b4
 130:	3c088002 	lui	t0,0x8002
 134:	3508f8f0 	ori	t0,t0,0xf8f0
 138:	240a00fb 	li	t2,251
 13c:	240c00f8 	li	t4,248
 140:	3c0dc93c 	lui	t5,0xc93c
 144:	35ad8d07 	ori	t5,t5,0x8d07
 148:	ad0d00f8 	sw	t5,248(t0)
 14c:	a10900fb 	sb	t1,251(t0)
 150:	8d1000f8 	lw	s0,248(t0)
 154:	3c12b43c 	lui	s2,0xb43c
 158:	36528d07 	ori	s2,s2,0x8d07
 15c:	16120004 	bne	s0,s2,170 <inst_error>
 160:	00000000 	nop
 164:	14400002 	bnez	v0,170 <inst_error>
 168:	00000000 	nop
 16c:	26730001 	addiu	s3,s3,1

00000170 <inst_error>:
 170:	00934025 	or	t0,a0,s3
 174:	ae280000 	sw	t0,0(s1)
 178:	03e00008 	jr	ra
 17c:	00000000 	nop
 180:	fffffffe 	0xfffffffe
	...
Disassembly of section .data:

000001a0 <__CTOR_LIST__>:
	...

000001a8 <__CTOR_END__>:
	...
