# encoding: utf8
"""
    输入机器码放在 ins.out.txt 中
    $ python decompile.py
    输出结果发到 ins.deass.txt 中

    加指令:
        实现函数
            指令名称(s)
        其中s是32位01串, s[0]为msb.
        不要直接访问s的位, 利用opcode(s), rs(s)等函数.
"""

def opcode(s):
    return s[0:6]

def rs(s):
    return "$%d" % int(s[6:11], 2)

def rt(s):
    return "$%d" % int(s[11:16], 2)

def rd(s):
    return "$%d" % int(s[16:21], 2)

def opfunc(s):
    return s[26:32]

def sa(s):
    return int(s[21:26], 2)

def imm16(s):
    return "0x" + hex(int(s[16:32], 2))[2:].rjust(4, "0")

#-----------------------------------------------------------------------------
# I type inst (EDIT)
#-----------------------------------------------------------------------------
def addiu(s):
    if opcode(s) == "001001":
        return "addiu %s, %s, %s" % (rt(s), rs(s), imm16(s))
    else:
        return None

def ori(s): # s is rev(inp)
    if opcode(s) == "001101":
        return "ori %s, %s, %s" % (rt(s), rs(s), imm16(s))
    else:
        return None

#-----------------------------------------------------------------------------
# R type inst (EDIT)
#-----------------------------------------------------------------------------
def subu(s):
    if opcode(s) == "000000" and opfunc(s) == "100011":
        return "subu %s, %s, %s" % (rd(s), rs(s), rt(s))
    else:
        return None

def slt(s):
    if opcode(s) == "000000" and opfunc(s) == "101010":
        return "slt %s, %s, %s" % (rd(s), rs(s), rt(s))
    else:
        return None

#-----------------------------------------------------------------------------
# DONT CHANGE
#-----------------------------------------------------------------------------

ops = dir()
i = 0
while i < len(ops):
    if ops[i].startswith("_"):
        del ops[i]
    elif ops[i] in {"opcode", "rs", "rt", "rd", "rd", "opfunc", "sa", "imm16", "deass"}:
        del ops[i]
    else:
        i += 1

def deass(s):
    for op in ops:
        cmd = op + "(s)"
        t = eval(cmd) 
        if t is not None:
            return t
    print("Error decompiling s (opcode: %s)." % opcode(s))
    exit()

with open("ins.out.txt", "r") as fin, open("ins.deass.txt", "w") as fout:
    for l in fin:
        s = bin(int(l, 16))[2:].rjust(32, "0")
        s = deass(s)
        fout.write(s+"\n")

